/*
 Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

#include <IsolationSelection/IsolationLowPtPLVTool.h>
#include <xAODBase/ObjectType.h>
#include <xAODBase/IParticleHelpers.h>

//Tools includes:
#include <cmath>
#include "PathResolver/PathResolver.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodBDT.h"

namespace CP {
  
  IsolationLowPtPLVTool::IsolationLowPtPLVTool(const std::string& toolName) :
    asg::AsgTool(toolName){
    // Decorator for scores
    //declareProperty( "augmentNominalPLV", m_augmentNominalPLV = false, "use this option to re-calculate nominal PLV score as well");
    declareProperty( "augmentNominalPLV", m_augmentNominalPLV = true, "use this option to re-calculate nominal PLV score as well");
    // LowPtPLV
    declareProperty( "MuonCalibFile", m_muonCalibFile = "IsolationCorrections/v5/TMVAClassification_BDT_Muon_LowPtPromptLeptonTagger_20191107.weights.xml", 
		     "XML file holding the TMVA configuration for muons" );
    declareProperty( "ElecCalibFile", m_elecCalibFile = "IsolationCorrections/v5/TMVAClassification_BDT_Electron_LowPtPromptLeptonTagger_20191107.weights.xml", 
		     "XML file holding the TMVA configuration for electrons");
    declareProperty( "MuonMethodName", m_muonMethodName = "LowPtPLT_Muon", "Method name for electron LowPtPLV" );
    declareProperty( "ElecMethodName", m_elecMethodName = "LowPtPLT_Elec", "Method name for muon LowPtPLV" );
    // PLV
    declareProperty( "MuonPLVCalibFile", m_muonPLVCalibFile = "JetTagNonPromptLepton/InputData-2017-10-27/Muon/PromptLeptonVeto/TMVAClassification_BDT_Muon_PromptLeptonVeto.weights.xml",
		     "XML file holding the TMVA configuration for nominal PLV, muons" );
    declareProperty( "ElecPLVCalibFile", m_elecPLVCalibFile = "JetTagNonPromptLepton/InputData-2017-10-27/Electron/PromptLeptonVeto/TMVAClassification_BDT_Electron_PromptLeptonVeto.weights.xml", 
		     "XML file holding the TMVA configuration for nominal PLV, electrons");
    declareProperty( "MuonPLVMethodName", m_muonPLVMethodName = "PLV_Muon", "Method name for electron nominal PLV" );
    declareProperty( "ElecPLVMethodName", m_elecPLVMethodName = "PLV_Elec", "Method name for muon nominal PLV" );
  }
  
  StatusCode IsolationLowPtPLVTool::initialize() {
    TMVA::Tools::Instance();
    m_TMVAReader_Muon = std::make_unique<TMVA::Reader>("!Silent:Color");
    m_TMVAReader_Elec = std::make_unique<TMVA::Reader>("!Silent:Color");

    std::string fullPathToFile_Muon = PathResolverFindCalibFile(m_muonCalibFile);
    std::string fullPathToFile_Elec = PathResolverFindCalibFile(m_elecCalibFile);

    static const std::array< std::string, N_VARIABLES > BDT_vars_Muon {
      "TrackJetNTrack",
      "PtRel",
      "PtFrac",
      "DRlj",
      "TopoEtCone20Rel",
      "PtVarCone30Rel"
	};

    static const std::array< std::string, N_VARIABLES > BDT_vars_Elec {
      "TrackJetNTrack",
      "PtRel",
      "PtFrac",
      "DRlj",
      "TopoEtCone20Rel",
      "PtVarCone20Rel"
    };

    for (int i=0; i<N_VARIABLES; i++){
      m_TMVAReader_Muon->AddVariable(BDT_vars_Muon[i], &m_varTMVA_Muon[i]);
    }

    for (int i=0; i<N_VARIABLES; i++){
      m_TMVAReader_Elec->AddVariable(BDT_vars_Elec[i], &m_varTMVA_Elec[i]);
    }

    if (fullPathToFile_Muon == ""){
      ATH_MSG_ERROR("Error! No xml file found for Muon LowPtPLV");
      return StatusCode::FAILURE;
    }
    m_TMVAReader_Muon->BookMVA(m_muonMethodName, fullPathToFile_Muon);
    TMVA::MethodBDT* method_Muon_bdt = dynamic_cast<TMVA::MethodBDT*> (m_TMVAReader_Muon->FindMVA(m_muonMethodName));
    if(!method_Muon_bdt){    
      ATH_MSG_ERROR("Error! No method found for Muon LowPtPLV");
      return StatusCode::FAILURE;
    }

    if (fullPathToFile_Elec == ""){
      ATH_MSG_ERROR("Error! No xml file found for Electron LowPtPLV");
      return StatusCode::FAILURE;
    }    m_TMVAReader_Elec->BookMVA(m_elecMethodName, fullPathToFile_Elec);
    TMVA::MethodBDT* method_Elec_bdt = dynamic_cast<TMVA::MethodBDT*> (m_TMVAReader_Elec->FindMVA(m_elecMethodName));
    if(!method_Elec_bdt){    
      ATH_MSG_ERROR("Error! No method found for Electron LowPtPLV");
      return StatusCode::FAILURE;
    }
    initializeNominalPLV();
    return StatusCode::SUCCESS;    
  }
  
  StatusCode IsolationLowPtPLVTool::initializeNominalPLV() {
    TMVA::Tools::Instance();
    m_TMVAReader_MuonPLV = std::make_unique<TMVA::Reader>("!Silent:Color");
    m_TMVAReader_ElecPLV = std::make_unique<TMVA::Reader>("!Silent:Color");
    std::string fullPathToFile_MuonPLV = PathResolverFindCalibFile(m_muonPLVCalibFile);
    std::string fullPathToFile_ElecPLV = PathResolverFindCalibFile(m_elecPLVCalibFile);

    static const std::array< std::string, N_PLV_VARIABLES > BDT_vars_PLV {
      "TrackJetNTrack",
      "rnnip",
      "DL1mu",
      "PtRel",
      "PtFrac",
      "DRlj",
      "TopoEtCone30Rel",
      "PtVarCone30Rel"
    };

    for (int i=0; i<N_PLV_VARIABLES; i++){
      m_TMVAReader_ElecPLV->AddVariable(BDT_vars_PLV[i], &m_varTMVA_PLV[i]);
      m_TMVAReader_MuonPLV->AddVariable(BDT_vars_PLV[i], &m_varTMVA_PLV[i]);
    }

    if (fullPathToFile_MuonPLV == ""){
      ATH_MSG_ERROR("Error! No xml file found for Muon PLV");
      return StatusCode::FAILURE;
    }
    m_TMVAReader_MuonPLV->BookMVA(m_muonPLVMethodName, fullPathToFile_MuonPLV);
    TMVA::MethodBDT* method_MuonPLV_bdt = dynamic_cast<TMVA::MethodBDT*> (m_TMVAReader_MuonPLV->FindMVA(m_muonPLVMethodName));
    if(!method_MuonPLV_bdt){    
      ATH_MSG_ERROR("Error! No method found for Muon PLV");
      return StatusCode::FAILURE;
    }

    if (fullPathToFile_ElecPLV == ""){
      ATH_MSG_ERROR("Error! No xml file found for Electron PLV");
      return StatusCode::FAILURE;
    }    
    m_TMVAReader_ElecPLV->BookMVA(m_elecPLVMethodName, fullPathToFile_ElecPLV);
    TMVA::MethodBDT* method_ElecPLV_bdt = dynamic_cast<TMVA::MethodBDT*> (m_TMVAReader_ElecPLV->FindMVA(m_elecPLVMethodName));
    if(!method_ElecPLV_bdt){    
      ATH_MSG_ERROR("Error! No method found for Electron LowPtPLV");
      return StatusCode::FAILURE;
    }
    
    ATH_MSG_INFO("Initialized Nominal PLV setups");
    return StatusCode::SUCCESS;
  }



  StatusCode IsolationLowPtPLVTool::augmentPLV( const xAOD::IParticle& particle ){
    return StatusCode::SUCCESS;
  }
    
  StatusCode IsolationLowPtPLVTool::augmentLowPtPLV( const float InputVal[9],float particlePt,int particleFlavor, float scoreOut[2]) {


    short TrackJetNTrack = InputVal[0];
    float DRlj         = InputVal[1];
    float PtRel        = InputVal[2];
    float PtFrac       = InputVal[3];
    float topoetcone20 = InputVal[4];
    float ptvarcone30  = InputVal[5];
    float ptvarcone20  = InputVal[6];

    float pt = particlePt;
    float score = 1.1;


    if (particleFlavor == 2){//Muon
      m_varTMVA_Muon[0] = TrackJetNTrack;
      m_varTMVA_Muon[1] = PtRel;
      m_varTMVA_Muon[2] = PtFrac;
      m_varTMVA_Muon[3] = DRlj;
      m_varTMVA_Muon[4] = topoetcone20/pt;
      m_varTMVA_Muon[5] = ptvarcone30/pt;
      std::cout<<"/// Muon ///"<<std::endl;
      score = m_TMVAReader_Muon->EvaluateMVA(m_muonMethodName);
    }
    else if (particleFlavor == 1){//Electron
      m_varTMVA_Elec[0] = TrackJetNTrack;
      m_varTMVA_Elec[1] = PtRel;
      m_varTMVA_Elec[2] = PtFrac;
      m_varTMVA_Elec[3] = DRlj;
      m_varTMVA_Elec[4] = topoetcone20/pt;
      m_varTMVA_Elec[5] = ptvarcone20/pt;
      std::cout<<"/// Elec ///"<<std::endl;
      score =  m_TMVAReader_Elec->EvaluateMVA(m_elecMethodName);
    }
    else {
      ATH_MSG_ERROR( "The function needs either a muon or an electron!" );
      return StatusCode::FAILURE;
    }
    scoreOut[0]=score;
    // Nominal PLV part starts here
    if (!m_augmentNominalPLV) return StatusCode::SUCCESS;
    float rnnip = InputVal[7];
    float DL1mu = InputVal[8];

    m_varTMVA_PLV[0] = TrackJetNTrack;
    m_varTMVA_PLV[1] = rnnip;
    m_varTMVA_PLV[2] = DL1mu;
    m_varTMVA_PLV[3] = PtRel;
    m_varTMVA_PLV[4] = PtFrac;
    m_varTMVA_PLV[5] = DRlj;
    m_varTMVA_PLV[6] = topoetcone20/pt; // we don't have topoetcone30,,,

    if (particleFlavor == 2){//Muon
      m_varTMVA_PLV[7] = ptvarcone30/pt;
      score = m_TMVAReader_MuonPLV->EvaluateMVA(m_muonPLVMethodName);
    }
    else if (particleFlavor == 1){//Electron
      m_varTMVA_PLV[7] = ptvarcone20/pt; // we don't have ptvarone30 for electrons,,,
      score =  m_TMVAReader_ElecPLV->EvaluateMVA(m_elecPLVMethodName);
    }
    else {
      ATH_MSG_ERROR( "The function needs either a muon or an electron!" );
      return StatusCode::FAILURE;
    }
    scoreOut[1] = score;

    return StatusCode::SUCCESS;
  }
}
