/*
 Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

#ifndef IsolationSelection_IsolationLowPtPLVTool_H
#define IsolationSelection_IsolationLowPtPLVTool_H

#include <IsolationSelection/IIsolationLowPtPLVTool.h>
#include <IsolationSelection/Defs.h>

#include <AsgTools/AsgTool.h>

// TMVA
#include "TMVA/Reader.h"

#include <memory>

namespace CP {
    class IsolationLowPtPLVTool: public asg::AsgTool, public virtual IIsolationLowPtPLVTool {

        public:
            IsolationLowPtPLVTool(const std::string& name);
	    ASG_TOOL_CLASS(IsolationLowPtPLVTool, IIsolationLowPtPLVTool)
            virtual StatusCode initialize() override;
            virtual StatusCode initializeNominalPLV();
	    virtual StatusCode augmentPLV( const xAOD::IParticle& particle ) override;
	    virtual StatusCode augmentLowPtPLV( const float InputVal[9],float particlePt,int particleType, float scoreOut[2] );
 


        private:
	    bool m_augmentNominalPLV = false;

	    std::string m_muonCalibFile = "";
	    std::string m_elecCalibFile = "";
	    std::string m_muonMethodName = "";
	    std::string m_elecMethodName = "";
	    std::unique_ptr<TMVA::Reader> m_TMVAReader_Muon;
	    std::unique_ptr<TMVA::Reader> m_TMVAReader_Elec;
	    static const int N_VARIABLES = 6;
	    float m_varTMVA_Muon[ N_VARIABLES ];
	    float m_varTMVA_Elec[ N_VARIABLES ];

	    std::string m_muonPLVCalibFile = "";
	    std::string m_elecPLVCalibFile = "";
	    std::string m_muonPLVMethodName = "";
	    std::string m_elecPLVMethodName = "";
	    std::unique_ptr<TMVA::Reader> m_TMVAReader_MuonPLV;
	    std::unique_ptr<TMVA::Reader> m_TMVAReader_ElecPLV;
	    static const int N_PLV_VARIABLES = 8;
	    float m_varTMVA_PLV[ N_PLV_VARIABLES ];
    };

}
#endif
