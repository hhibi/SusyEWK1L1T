#ifndef EWK1L1TANALYSIS_h
#define EWK1L1TANALYSIS_h

class HistogramManager;
class SUSYObj;
class tree_NoSys;
class LeptonObjects;
class EWK1L1TAnalysis{

 public:
  EWK1L1TAnalysis(tree_NoSys *tree){m_tree=tree;}
  void doAnalysis(HistogramManager &hs,const SUSYObj N2,const SUSYObj C1,LeptonObjects Leps,LeptonObjects SigLeps); 

  bool cut(LeptonObjects Leps,LeptonObjects SigLeps)const;

 private:
  tree_NoSys *m_tree;

};

#endif
