#ifndef EWK1L2TANALYSIS_h
#define EWK1L2TANALYSIS_h

class HistogramManager;
class SUSYObj;
class tree_NoSys;
class LeptonObjects;
class EWK1L2TAnalysis{

 public:
  EWK1L2TAnalysis(tree_NoSys *tree){m_tree=tree;}
  void doAnalysis(HistogramManager &hs,const SUSYObj N2,const SUSYObj C1,LeptonObjects Leps,LeptonObjects SigLeps); 

  bool cut(LeptonObjects Leps,LeptonObjects SigLeps,HistogramManager &hs)const;

 private:
  tree_NoSys *m_tree;

};

#endif
