#ifndef TRACKANALYSIS_h
#define TRACKANALYSIS_h

#include <map>

class HistogramManager;
class BranchManager;
class SUSYObj;
class tree_NoSys;
class LeptonObjects;
class ReadBDTB;
class  ReadBDTB_TrackSelection;
class TH1F;

class TrackAnalysis{

 public:
  TrackAnalysis(tree_NoSys *tree,double MassCUT);
  ~TrackAnalysis();

  void doAnalysis(HistogramManager &hs,const SUSYObj N2,const SUSYObj C1,LeptonObjects Leps,LeptonObjects SigLeps); 
  bool doTrackSelection(HistogramManager &hs,BranchManager &br,const SUSYObj N2,const SUSYObj C1,LeptonObjects Leps,LeptonObjects SigLeps, std::string CombiSign,int &NumOfZcan,std::string MCorDATA, TH1F *dRcorre); 

  void exactlyOneZcanAnalysis(HistogramManager &hs, bool IsSUSY);


  bool TrackCutDef(HistogramManager &hs,int trkIndex,LeptonObjects SigLeps)const;
  bool TrackCutDef(int trkIndex,LeptonObjects SigLeps)const;
 private:
 
  tree_NoSys *m_tree;
  ReadBDTB *track_BDTB;
  ReadBDTB_TrackSelection *trkSelection_BDTB;
  double m_MassCUT;
  double m_BDTBscore_OneZ;
  std::map<std::string, int> m_CombiSignConfig;


};

#endif
