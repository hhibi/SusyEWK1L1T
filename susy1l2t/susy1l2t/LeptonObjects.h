#ifndef LEPTONOBJECTS_h
#define LEPTONOBJECTS_h

#include <iostream>
#include <vector>
#include "SUSYObjTools.h"

class LeptonVariable;
class tree_NoSys;

class LeptonObjects{

 public:

  LeptonObjects(tree_NoSys *tree);
  void erase();


  //-- getter--
  LeptonVariable* getLepton(int index)const{return (*m_Lep)[index];}
  std::vector<LeptonVariable*> getLeptons()const{return *m_Lep;}
  int getNumberOfLepton()const{return m_Lep->size();}   

  //--setter--
  void setLepton(bool IsSignalReq,SUSYObjTools::SUSY_PID SUSYpid=SUSYObjTools::SUSY_PID::NoSUSY);


  //--tools
  bool IsSameFlavor();
  bool IsOppositeSign();

 private:
  std::vector<LeptonVariable*> *m_Lep;
  tree_NoSys *m_tree;

};

#endif
