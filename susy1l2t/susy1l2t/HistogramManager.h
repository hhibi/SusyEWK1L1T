#ifndef HISTOGRAMMANAGER_h
#define HISTOGRAMMANAGER_h

#include <iostream>



#include <TH2D.h>
#include <TH1D.h>
#include <TFile.h>

//class TH2D;
//class TH1D;
//class TFile;

class HistogramManager{

 public:
  HistogramManager(std::string FILE, std::string CombiSign);
  ~HistogramManager();

  TFile *file;
  TH1D* dR_N2_Z;
  TH1D* dR_N2_Z_Reco;
  TH2D* dR_dRlep;
  TH2D* dPhi_JetPt;

  TH1D* Reco_leppT_fromW; 
  TH1D* Truth_leppT_fromW;
  TH2D* NBaseLepton_OriginFlavor;
  TH1D* Zmass_1L1T;
  TH1D* Zmass_1L2T;
  TH1D* Zmass_1L1T_ALL;
  TH1D* Zmass_1L2T_ALL;

  TH1D *DeltaPhi_MetLep;
  TH2D *TruthEta_N2 ;
  TH2D *TruthPhi_N2 ;
  TH2D *TruthEtaPhi_N2;
  TH1D *dEta_Tracks ;
  TH1D *dPhi_Tracks ;
  TH1D *dPt_Tracks;

  TH1D *DPHI_METLep_Z;
  TH1D *DPHI_METLep_W;

  TH2D *PP;

  TH2D *MinMaxDR_S;
  TH2D *MinMaxDR_B;

  TH2D *HitMap_S;
  TH2D *HitMap_B;

  TH2D *HitMapW_S;
  TH2D *HitMapW_B;

  TH1D *ZisolationPt_S[10];
  TH1D *ZisolationN_S[10];
  TH2D *ZisolationPtN_S[10];
  TH1D *ZisolationPt_B[10];
  TH1D *ZisolationN_B[10];
  TH2D *ZisolationPtN_B[10];

  TH1D *LepIsolationPt_S[10];
  TH1D *LepIsolationN_S[10];
  TH1D *LepIsolationPt_B[10];
  TH1D *LepIsolationN_B[10];

  TH1D *TrkIsolationPt_S[10];
  TH1D *TrkIsolationN_S[10];
  TH1D *TrkIsolationPt_B[10];
  TH1D *TrkIsolationN_B[10];
 
  TH1D *A;
  TH1D *B;
  TH1D *C;
  TH1D *CC;
  TH1D *D;
  TH1D *E;
  TH1D *F;

  TH2D *AA;
  TH2D *BB;
  TH2D *DD;

  TH1D *NCombi;
  TH1D *NCombi_CUT;
  TH1D *NCombi_CUT_IsSUSY;

  TH1D *NZCombi;
  TH1D *NZCombi_CUT;


  TH2D *Z_MET_Phi_S;
  TH2D *Z_JET_Phi_S;
  TH2D *Z_JETMET_Phi_S;
  TH2D *Z_MET_Phi_B;
  TH2D *Z_JET_Phi_B;
  TH2D *Z_JETMET_Phi_B;

  TH1D *Z_MET_DPhi_S;
  TH1D *Z_JET_DPhi_S;
  TH1D *Z_Mass_S;
  TH1D *Z_JETMET_DPhi_S;
  TH1D *Z_dR_LepTrk_S;
  TH2D *DPhi_METZ_JETPt_S;

  TH1D *Z_MET_DPhi_B;
  TH1D *Z_JET_DPhi_B;
  TH1D *Z_Mass_B;
  TH1D *Z_JETMET_DPhi_B;
  TH1D *Z_dR_LepTrk_B;
  TH2D *DPhi_METZ_JETPt_B;

  TH1D *DPhi_METJET_S;
  TH1D *DPhi_METJET_B;

  TH1D *Z0sin_S;
  TH1D *D0sig_S;
  TH1D *Z0sin_B;
  TH1D *D0sig_B;
  TH2D *pTpT_S;
  TH2D *pTpT_B;

  double WEIGHT;


  TH1D *EVENT_CUT;
  TH1D *EVENT_CUT2;
  TH1D *Zcan_CUT;
  TH1D *Zcan_CUT_IsSUSY;
  TH1D *Zcan_CUT2;
  TH1D *Zcan_CUT2_IsSUSY;


  TH2D *ZpT_dR_S;
  TH2D *ZpT_dR_B;


  TH2D *Zp_dR_S;
  TH2D *Zp_dR_B;

  TH2D *H_MET_S;
  TH2D *H_MET_B;
  TH1D *ratio_METH_S; 
  TH1D *ratio_METH_B;


  TH1D *recoZmass_S;
  TH1D *recoZmass_B;

  TH1D *MindR_TrkJet_S;
  TH1D *MindR_TrkJet_B;

  TH1D *MindR_LepJet_S;
  TH1D *MindR_LepJet_B;


  TH1D *ReconType;



  TH1D *truth_HigerpT_N2;
  TH1D *truth_LowerpT_N2;

  TH1D *dR_N2;


  TH1D *BDTBScore_S;
  TH1D *BDTBScore_B;


  TH1D *BDTBScore_OneZ_S;
  TH1D *BDTBScore_OneZ_B;


  TH2D *BDTBScore_MetTrackPt_m08;
  TH2D *BDTBScore_MetTrackPt_p08;

  TH1D* EMTTrackpT_S;
  TH1D* EMTTrackpT_B;


  TH1D* NTracks_S;
  TH1D* NTracks_B;

  TH1D* METEt_S;
  TH1D* METEt_B;
  TH1D* JETEt_S;
  TH1D* JETEt_B;

  TH1D* METPhi_S;
  TH1D* METPhi_B;
  TH1D* JETPhi_S;
  TH1D* JETPhi_B;
  TH1D* LeptonType_S;
  TH1D* LeptonType_B;

  TH2D* METPhi_DPhi_B;
  TH2D* JETPhi_DPhi_B;

  TH1D* pT_reco;
  TH1D* pT_unreco;

  TH1D* eta_reco;
  TH1D* eta_unreco;


  TH2D* pTeta_reco;
  TH2D* pTeta_unreco;

  TH2D* dRLepTrk_PromptLepVeto_S;
  TH2D* dRLepTrk_LowPtPLV_score_S;
  TH2D* dRLepTrk_LowPtPLV_scoreCorr_S;

  TH2D* dRLepTrk_PromptLepVeto_B;
  TH2D* dRLepTrk_LowPtPLV_score_B;
  TH2D* dRLepTrk_LowPtPLV_scoreCorr_B;

  TH2D* trackPtcone20_S;  
  TH2D* trackPtcone30_S;   
  TH2D* trackPtcone40_S;   
  TH2D* trackEtcone20Topo_S;    
  TH2D* trackEtcone40Topo_S;

  TH2D* trackPtcone20Corr_S;   
  TH2D* trackPtcone30Corr_S;   
  TH2D* trackPtcone40Corr_S;   
  TH2D* trackEtcone20TopoCorr_S;    
  TH2D* trackEtcone40TopoCorr_S;

  TH2D* trackPtcone20_B;  
  TH2D* trackPtcone30_B;   
  TH2D* trackPtcone40_B;   
  TH2D* trackEtcone20Topo_B;    
  TH2D* trackEtcone40Topo_B;

  TH2D* trackPtcone20Corr_B;   
  TH2D* trackPtcone30Corr_B;   
  TH2D* trackPtcone40Corr_B;   
  TH2D* trackEtcone20TopoCorr_B;    
  TH2D* trackEtcone40TopoCorr_B;

 private:


  std::string m_FileName;


};

#endif
