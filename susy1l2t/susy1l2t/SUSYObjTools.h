#ifndef SUSYOBJTOOLS_h
#define SUSYOBJTOOLS_h

#include <vector>

#include <TLorentzVector.h>

#include "TrackVariable.h"
#include "LeptonVariable.h"
//#include "LeptonObjects.h"
#include "TrackAnalysis.h"
class tree_NoSys;
class SUSYObj;
class LeptonObjects;

class SUSYObjTools{

 public:
  SUSYObjTools(tree_NoSys *tree,double MassCUT){
    m_tree=tree;
    m_trkAnalysis = new TrackAnalysis(m_tree,MassCUT);
  }

  ~SUSYObjTools(){
    delete m_trkAnalysis;
  }



  enum SUSY_PID{NoSUSY=0,N1=1000022,N2,C1};

  void setSUSYInfo(SUSY_PID SUSYpid,SUSYObj &susyObj,LeptonObjects SigLeps);

  void setLeptonInfo(SUSY_PID SUSYpid,SUSYObj &susyObj);
  void setTrackInfo(SUSY_PID SUSYpid,SUSYObj &susyObj,LeptonObjects SigLeps);
  void setTruthParticleInfo(SUSY_PID SUSYpid,SUSYObj &susyObj);

  double setMass(int flavor);

 private:
  tree_NoSys *m_tree;
  TrackAnalysis *m_trkAnalysis;
};

#endif
