#ifndef EWK0L2TANALYSIS_h
#define EWK0L2TANALYSIS_h

#include <map>

class HistogramManager;
class BranchManager;
class SUSYObj;
class tree_NoSys;
class LeptonObjects;

class EWK0L2TAnalysis{

 public:
  EWK0L2TAnalysis(tree_NoSys *tree,double MassCUT);
  ~EWK0L2TAnalysis();

  bool doAnalysis(HistogramManager &hs,BranchManager &br,const SUSYObj N2,const  SUSYObj C1,LeptonObjects Leps,LeptonObjects SigLeps, std::string CombiSign, int &NumOfZcan, std::string MCorDATA);
  bool doTrackSelection(HistogramManager &hs,BranchManager &br,const SUSYObj N2,const SUSYObj C1,LeptonObjects Leps,LeptonObjects SigLeps, std::string CombiSign,int &NumOfZcan,std::string MCorDATA); 

  void exactlyOneZcanAnalysis(HistogramManager &hs, bool IsSUSY);

  bool cut(LeptonObjects Leps,LeptonObjects SigLeps,HistogramManager &hs)const;

  bool TrackCutDef(HistogramManager &hs,int trkIndex)const;

 private:
 
  tree_NoSys *m_tree;
  double m_MassCUT;
  double m_BDTBscore_OneZ;
  std::map<std::string, int> m_CombiSignConfig;


};

#endif
