#ifndef SusySkimMaker_LeptonVariable_h
#define SusySkimMaker_LeptonVariable_h
#include "TrackVariable.h"

class LeptonVariable : public TrackVariable
{

 public:
  LeptonVariable();
  virtual ~LeptonVariable(){};

  LeptonVariable(const LeptonVariable&);
  LeptonVariable& operator=(const LeptonVariable&);

  ///
  /// Primary author for this lepton
  ///
  int author;

  ///
  /// Isolation variable: Sum of the topological cluster ET within a cone of a radius dR = 0.2
  ///
  float topoetcone20;
  
  ///
  /// Isolation variable: Sum of the topological cluster ET within a cone of a radius dR = 0.3
  ///
  float topoetcone30;
  
  ///
  /// Isolation variable:Sum of the topological cluster ET within a cone of a radius dR = 0.4;
  ///
  float topoetcone40;

  ///
  /// Isolation variables: For pT>50 GeV, ptvarcone has a cone of dr=0.2;
  ///
  float ptvarcone20;

  ///
  /// Isolation variables: For pT>33.3 GeV, ptvarcone has a cone of dr=0.3;
  ///
  float ptvarcone30;

  ///
  /// Isolation variables: For pT>25 GeV, ptvarcone has a cone of dr=0.4;
  ///
  float ptvarcone40;

  ///
  /// Isolation variables: pTvarcone calculated with tracks passing the standard d0Sig, Z0SinTheta cuts ("Track-To-Vertex-Associon", TTVA). Available in the derivations with ptag p3517+.
  ///
  float ptvarcone30_TightTTVA_pt1000;
  float ptvarcone30_TightTTVA_pt500;

  ///
  /// Isolation variables: Particle flow cone iso variable.
  ///
  float neflowisol20;

  // Isolation flags (WPs used in Rel 20.7 and early Rel21 analyses)
  // => https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/IsolationSelectionTool#WPs_used_for_Rel20_7_studies_and
  bool IsoLoose;
  bool IsoTight;
  bool IsoGradient;
  bool IsoLooseTrackOnly;
  bool IsoGradientLoose;
  bool IsoFixedCutLoose;
  bool IsoFixedCutTight;
  bool IsoFixedCutTightTrackOnly;


  // Isolation flags (New Rel21 WPs using the TTMVA tracks)
  // => https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/IsolationSelectionTool#Current_official_working_points
  bool IsoFCHighPtCaloOnly;
  bool IsoFixedCutHighPtTrackOnly;
  bool IsoFCTightTrackOnly;
  bool IsoFCLoose;
  bool IsoFCTight;
  bool IsoFCLoose_FixedRad;
  bool IsoFCTight_FixedRad;


  //PLV Isolation
  float PromptLepVeto_score;
  float LowPtPLV_score;

  float PLTInput_TrackJetNTrack;
  float PLTInput_DRlj;
  float PLTInput_PtRel;
  float PLTInput_PtFrac;
  float PLTInput_Topoetcone30Rel;
  float PLTInput_Topoetcone20Rel;
  float PLTInput_Ptvarcone30Rel;
  float PLTInput_Ptvarcone20Rel;
  float PLTInput_rnnip;
  float PLTInput_DL1mu;

  //new isolation WPs for compressed ewk 
  //bool IsoFixedCutHighMuTight;
  //bool IsoFixedCutHighMuLoose;
  //bool IsoFixedCutHighMuTrackOnly;
  //bool IsoFixedCutPflowTight;
  //bool IsoFixedCutPflowLoose;
  //bool IsoFixedCutHighPtCaloOnly;
  //bool IsoFixedCutTrackCone40;

  // For the NearbyLepIsoCorr versions of the iso vars
  float corr_ptcone20;
  float corr_ptcone30;
  float corr_ptcone40;
  float corr_topoetcone20;
  float corr_topoetcone30;
  float corr_topoetcone40;
  float corr_ptvarcone20;
  float corr_ptvarcone30;
  float corr_ptvarcone40;
  float corr_ptvarcone30_TightTTVA_pt1000;
  float corr_ptvarcone30_TightTTVA_pt500;
  float corr_neflowisol20;
  bool  corr_IsoLoose;
  bool  corr_IsoTight;
  bool  corr_IsoGradient;
  bool  corr_IsoLooseTrackOnly;
  bool  corr_IsoGradientLoose;
  bool  corr_IsoFixedCutLoose;
  bool  corr_IsoFixedCutTight;
  bool  corr_IsoFixedCutTightTrackOnly;
  bool  corr_IsoFCHighPtCaloOnly;
  bool  corr_IsoFixedCutHighPtTrackOnly;
  bool  corr_IsoFCTightTrackOnly;
  bool  corr_IsoFCLoose;
  bool  corr_IsoFCTight;
  bool  corr_IsoFCLoose_FixedRad;
  bool  corr_IsoFCTight_FixedRad;

  ///
  /// Track associated with the lepton
  ///
  TLorentzVector trackTLV;

  ///
  /// Save the truth type and origin for leptons. If this cannot be found, its defaulted to -999.
  /// Truth variables, the codes can be found here:
  ///   => https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/MCTruthClassifier/trunk/MCTruthClassifier/MCTruthClassifierDefs.h#L24
  ///

  float pTErr;

  //bool signal;
  bool passOR;
  bool baseline;

  ///
  /// Track Pixel and IBL hits
  ///
  uint8_t nPixHits;
  uint8_t nIBLHits;

  ///
  /// Charge of the truth lepton
  ///
  int truthQ;

  ///
  /// ElectronVariable and MuonVariable implement these for the IDs
  ///
  virtual bool passesTight()     const { return false; }
  virtual bool passesMedium()    const { return false; }
  virtual bool passesLoose()     const { return false; }
  virtual bool passesVeryLoose() const { return false; }

  /*
  ///
  /// Reconstruction efficiency scale factor, key=systematic name
  ///
  std::map<TString,float> recoSF;

  ///
  /// Trigger efficiencies
  ///
  MCCorrContainer trigEff;
  */

  ///
  /// Method to get the reconstruction SF for a given systematic
  ///
  float getRecoSF(const TString sys); 

  virtual bool isEle() const { return false; } //!
  virtual bool isMu()  const { return false; } //!

  /*
  ///
  /// Truth link
  ///
  mutable TruthVariable* truthLink;


  bool hasTruthLink(); //!
  */


  ///
  /// PromptLeptonTagger variables
  /// 
  int   plt_input_TrackJetNTrack;
  float plt_input_DRlj;
  float plt_input_rnnip;
  float plt_input_DL1mu;
  float plt_input_PtRel;
  float plt_input_PtFrac;
  float plt_input_Topoetcone30Rel;
  float plt_input_Ptvarcone30Rel;
  float promptLepIso_score;
  float promptLepVeto_score;

};

#endif
