//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan 16 20:56:09 2020 by ROOT version 6.16/00
// from TTree tree_NoSys/tree_NoSys
// found on file: SUSY.C1pN2.100p0_99p0.SUSY19.01.r10201.root
//////////////////////////////////////////////////////////

#ifndef tree_NoSys_h
#define tree_NoSys_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>

#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationSelection/IsolationLowPtPLVTool.h"


// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class tree_NoSys {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        trigWeight_metTrig;
   Bool_t          trigMatch_metTrig;
   Double_t        trigWeight_singleMuonTrig;
   Bool_t          trigMatch_singleMuonTrig;
   Double_t        trigWeight_singleElectronTrig;
   Bool_t          trigMatch_singleElectronTrig;
   Int_t           nLep_base;
   Int_t           nLep_signal;
   Int_t           nEle_signal;
   Int_t           nMu_signal;
   Int_t           nLep_signal_PLVLooseLoose;
   Int_t           nLep_signal_PLVLooseTight;
   Int_t           nLep_signal_PLVTightLoose;
   Int_t           nLep_signal_PLVTightTight;
   Int_t           lep1Flavor;
   Int_t           lep1OriginFlavor;
   Int_t           lep1Charge;
   Int_t           lep1Author;
   Float_t         lep1Pt;
   Float_t         lep1Eta;
   Float_t         lep1Phi;
   Float_t         lep1M;
   Float_t         lep1D0;
   Float_t         lep1Z0;
   Float_t         lep1D0Sig;
   Float_t         lep1Z0SinTheta;
   Float_t         lep1Z0OriginV;
   Float_t         lep1OriginVz;     
   Float_t         lep1Ptcone20;
   Float_t         lep1Ptcone30;
   Float_t         lep1Topoetcone20;
   Float_t         lep1Topoetcone30;
   Float_t         lep1Ptvarcone20;
   Float_t         lep1Ptvarcone30;
   Bool_t          lep1PassOR;
   Int_t           lep1Type;
   Int_t           lep1Origin;
   Bool_t          lep1PassBL;
   Bool_t          lep1VeryLoose;
   Bool_t          lep1Loose;
   Bool_t          lep1Medium;
   Bool_t          lep1Tight;
   Bool_t          lep1LowPtWP;
   Float_t         lep1minDRAllJetsLepton;
   Bool_t          lep1IsoCorrLoose;
   Bool_t          lep1IsoCorrTight;
   Bool_t          lep1IsoCorrGradient;
   Bool_t          lep1IsoCorrGradientLoose;
   Bool_t          lep1IsoCorrLooseTrackOnly;
   Bool_t          lep1IsoCorrFixedCutLoose;
   Bool_t          lep1IsoCorrFixedCutTight;
   Bool_t          lep1IsoCorrFixedCutTightTrackOnly;
   Bool_t          lep1IsoCorrFCTightTrackOnly;
   Bool_t          lep1IsoCorrPLVLoose;
   Bool_t          lep1IsoCorrPLVTight;
   Bool_t          lep1Signal;
   Bool_t          lep1SignalPLVLoose;
   Bool_t          lep1SignalPLVTight;
   Bool_t          lep1TruthMatched;
   Float_t         lep1PromptLepVeto_score;
   Float_t         lep1LowPtPLV_score;
   Float_t         lep1PLTInput_TrackJetNTrack;
   Float_t         lep1PLTInput_DRlj;
   Float_t         lep1PLTInput_rnnip;
   Float_t         lep1PLTInput_DL1mu;
   Float_t         lep1PLTInput_PtRel;
   Float_t         lep1PLTInput_PtFrac;
   Float_t         lep1PLTInput_Topoetcone30Rel;
   Float_t         lep1PLTInput_Ptvarcone30Rel;
   Float_t         lep1CorrPromptLepVeto_score;
   Float_t         lep1CorrLowPtPLV_score;
   Float_t         lep1CorrPLTInput_TrackJetNTrack;
   Float_t         lep1CorrPLTInput_DRlj;
   Float_t         lep1CorrPLTInput_rnnip;
   Float_t         lep1CorrPLTInput_DL1mu;
   Float_t         lep1CorrPLTInput_PtRel;
   Float_t         lep1CorrPLTInput_PtFrac;
   Float_t         lep1CorrPLTInput_Topoetcone30Rel;
   Float_t         lep1CorrPLTInput_Ptvarcone30Rel;
   Int_t           lep2Flavor;
   Int_t           lep2OriginFlavor;
   Int_t           lep2Charge;
   Int_t           lep2Author;
   Float_t         lep2Pt;
   Float_t         lep2Eta;
   Float_t         lep2Phi;
   Float_t         lep2M;
   Float_t         lep2D0;
   Float_t         lep2Z0;
   Float_t         lep2D0Sig;
   Float_t         lep2Z0SinTheta;
   Float_t         lep2Z0OriginV;
   Float_t         lep2OriginVz;
   Float_t         lep2Ptcone20;
   Float_t         lep2Ptcone30;
   Float_t         lep2Topoetcone20;
   Float_t         lep2Topoetcone30;
   Float_t         lep2Ptvarcone20;
   Float_t         lep2Ptvarcone30;
   Bool_t          lep2PassOR;
   Int_t           lep2Type;
   Int_t           lep2Origin;
   Bool_t          lep2PassBL;
   Bool_t          lep2VeryLoose;
   Bool_t          lep2Loose;
   Bool_t          lep2Medium;
   Bool_t          lep2Tight;
   Bool_t          lep2LowPtWP;
   Float_t         lep2minDRAllJetsLepton;
   Bool_t          lep2IsoCorrLoose;
   Bool_t          lep2IsoCorrTight;
   Bool_t          lep2IsoCorrGradient;
   Bool_t          lep2IsoCorrGradientLoose;
   Bool_t          lep2IsoCorrLooseTrackOnly;
   Bool_t          lep2IsoCorrFixedCutLoose;
   Bool_t          lep2IsoCorrFixedCutTight;
   Bool_t          lep2IsoCorrFixedCutTightTrackOnly;
   Bool_t          lep2IsoCorrFCTightTrackOnly;
   Bool_t          lep2IsoCorrPLVLoose;
   Bool_t          lep2IsoCorrPLVTight;
   Bool_t          lep2Signal;
   Bool_t          lep2SignalPLVLoose;
   Bool_t          lep2SignalPLVTight;
   Bool_t          lep2TruthMatched;
   Float_t         lep2PromptLepVeto_score;
   Float_t         lep2LowPtPLV_score;
   Float_t         lep2PLTInput_TrackJetNTrack;
   Float_t         lep2PLTInput_DRlj;
   Float_t         lep2PLTInput_rnnip;
   Float_t         lep2PLTInput_DL1mu;
   Float_t         lep2PLTInput_PtRel;
   Float_t         lep2PLTInput_PtFrac;
   Float_t         lep2PLTInput_Topoetcone30Rel;
   Float_t         lep2PLTInput_Ptvarcone30Rel;
   Float_t         lep2CorrPromptLepVeto_score;
   Float_t         lep2CorrLowPtPLV_score;
   Float_t         lep2CorrPLTInput_TrackJetNTrack;
   Float_t         lep2CorrPLTInput_DRlj;
   Float_t         lep2CorrPLTInput_rnnip;
   Float_t         lep2CorrPLTInput_DL1mu;
   Float_t         lep2CorrPLTInput_PtRel;
   Float_t         lep2CorrPLTInput_PtFrac;
   Float_t         lep2CorrPLTInput_Topoetcone30Rel;
   Float_t         lep2CorrPLTInput_Ptvarcone30Rel;
   Int_t           lep3Flavor;
   Int_t           lep3OriginFlavor;
   Int_t           lep3Charge;
   Int_t           lep3Author;
   Float_t         lep3Pt;
   Float_t         lep3Eta;
   Float_t         lep3Phi;
   Float_t         lep3M;
   Float_t         lep3D0;
   Float_t         lep3Z0;
   Float_t         lep3D0Sig;
   Float_t         lep3Z0SinTheta;
   Float_t         lep3Z0OriginV;
   Float_t         lep3OriginVz;
   Float_t         lep3Ptcone20;
   Float_t         lep3Ptcone30;
   Float_t         lep3Topoetcone20;
   Float_t         lep3Topoetcone30;
   Float_t         lep3Ptvarcone20;
   Float_t         lep3Ptvarcone30;
   Bool_t          lep3PassOR;
   Int_t           lep3Type;
   Int_t           lep3Origin;
   Bool_t          lep3PassBL;
   Bool_t          lep3VeryLoose;
   Bool_t          lep3Loose;
   Bool_t          lep3Medium;
   Bool_t          lep3Tight;
   Bool_t          lep3LowPtWP;
   Float_t         lep3minDRAllJetsLepton;
   Bool_t          lep3IsoCorrLoose;
   Bool_t          lep3IsoCorrTight;
   Bool_t          lep3IsoCorrGradient;
   Bool_t          lep3IsoCorrGradientLoose;
   Bool_t          lep3IsoCorrLooseTrackOnly;
   Bool_t          lep3IsoCorrFixedCutLoose;
   Bool_t          lep3IsoCorrFixedCutTight;
   Bool_t          lep3IsoCorrFixedCutTightTrackOnly;
   Bool_t          lep3IsoCorrFCTightTrackOnly;
   Bool_t          lep3IsoCorrPLVLoose;
   Bool_t          lep3IsoCorrPLVTight;
   Bool_t          lep3Signal;
   Bool_t          lep3SignalPLVLoose;
   Bool_t          lep3SignalPLVTight;
   Bool_t          lep3TruthMatched;
   Float_t         lep3PromptLepVeto_score;
   Float_t         lep3LowPtPLV_score;
   Float_t         lep3PLTInput_TrackJetNTrack;
   Float_t         lep3PLTInput_DRlj;
   Float_t         lep3PLTInput_rnnip;
   Float_t         lep3PLTInput_DL1mu;
   Float_t         lep3PLTInput_PtRel;
   Float_t         lep3PLTInput_PtFrac;
   Float_t         lep3PLTInput_Topoetcone30Rel;
   Float_t         lep3PLTInput_Ptvarcone30Rel;
   Float_t         lep3CorrPromptLepVeto_score;
   Float_t         lep3CorrLowPtPLV_score;
   Float_t         lep3CorrPLTInput_TrackJetNTrack;
   Float_t         lep3CorrPLTInput_DRlj;
   Float_t         lep3CorrPLTInput_rnnip;
   Float_t         lep3CorrPLTInput_DL1mu;
   Float_t         lep3CorrPLTInput_PtRel;
   Float_t         lep3CorrPLTInput_PtFrac;
   Float_t         lep3CorrPLTInput_Topoetcone30Rel;
   Float_t         lep3CorrPLTInput_Ptvarcone30Rel;
   Int_t           lep4Flavor;
   Int_t           lep4OriginFlavor;
   Int_t           lep4Charge;
   Int_t           lep4Author;
   Float_t         lep4Pt;
   Float_t         lep4Eta;
   Float_t         lep4Phi;
   Float_t         lep4M;
   Float_t         lep4D0;
   Float_t         lep4Z0;
   Float_t         lep4D0Sig;
   Float_t         lep4Z0SinTheta;
   Float_t         lep4Z0OriginV;
   Float_t         lep4OriginVz;
   Float_t         lep4Ptcone20;
   Float_t         lep4Ptcone30;
   Float_t         lep4Topoetcone20;
   Float_t         lep4Topoetcone30;
   Float_t         lep4Ptvarcone20;
   Float_t         lep4Ptvarcone30;
   Bool_t          lep4PassOR;
   Int_t           lep4Type;
   Int_t           lep4Origin;
   Bool_t          lep4PassBL;
   Bool_t          lep4VeryLoose;
   Bool_t          lep4Loose;
   Bool_t          lep4Medium;
   Bool_t          lep4Tight;
   Bool_t          lep4LowPtWP;
   Float_t         lep4minDRAllJetsLepton;
   Bool_t          lep4IsoCorrLoose;
   Bool_t          lep4IsoCorrTight;
   Bool_t          lep4IsoCorrGradient;
   Bool_t          lep4IsoCorrGradientLoose;
   Bool_t          lep4IsoCorrLooseTrackOnly;
   Bool_t          lep4IsoCorrFixedCutLoose;
   Bool_t          lep4IsoCorrFixedCutTight;
   Bool_t          lep4IsoCorrFixedCutTightTrackOnly;
   Bool_t          lep4IsoCorrFCTightTrackOnly;
   Bool_t          lep4IsoCorrPLVLoose;
   Bool_t          lep4IsoCorrPLVTight;
   Bool_t          lep4Signal;
   Bool_t          lep4SignalPLVLoose;
   Bool_t          lep4SignalPLVTight;
   Bool_t          lep4TruthMatched;
   Float_t         lep4PromptLepVeto_score;
   Float_t         lep4LowPtPLV_score;
   Float_t         lep4PLTInput_TrackJetNTrack;
   Float_t         lep4PLTInput_DRlj;
   Float_t         lep4PLTInput_rnnip;
   Float_t         lep4PLTInput_DL1mu;
   Float_t         lep4PLTInput_PtRel;
   Float_t         lep4PLTInput_PtFrac;
   Float_t         lep4PLTInput_Topoetcone30Rel;
   Float_t         lep4PLTInput_Ptvarcone30Rel;
   Float_t         lep4CorrPromptLepVeto_score;
   Float_t         lep4CorrLowPtPLV_score;
   Float_t         lep4CorrPLTInput_TrackJetNTrack;
   Float_t         lep4CorrPLTInput_DRlj;
   Float_t         lep4CorrPLTInput_rnnip;
   Float_t         lep4CorrPLTInput_DL1mu;
   Float_t         lep4CorrPLTInput_PtRel;
   Float_t         lep4CorrPLTInput_PtFrac;
   Float_t         lep4CorrPLTInput_Topoetcone30Rel;
   Float_t         lep4CorrPLTInput_Ptvarcone30Rel;
   Int_t           N2_particle0_Flavor;
   Int_t           N2_particle1_Flavor;
   Int_t           N2_particle2_Flavor;
   Int_t           N2_particle3_Flavor;
   Float_t         N2_particle0_Eta;
   Float_t         N2_particle1_Eta;
   Float_t         N2_particle2_Eta;
   Float_t         N2_particle3_Eta;
   Float_t         N2_particle0_Phi;
   Float_t         N2_particle1_Phi;
   Float_t         N2_particle2_Phi;
   Float_t         N2_particle3_Phi;
   Float_t         N2_particle0_pT;
   Float_t         N2_particle1_pT;
   Float_t         N2_particle2_pT;
   Float_t         N2_particle3_pT;
   Float_t         N2_particle0_VtxX;
   Float_t         N2_particle1_VtxX;
   Float_t         N2_particle2_VtxX;
   Float_t         N2_particle3_VtxX;
   Float_t         N2_particle0_VtxY;
   Float_t         N2_particle1_VtxY;
   Float_t         N2_particle2_VtxY;
   Float_t         N2_particle3_VtxY;
   Float_t         N2_particle0_VtxZ;
   Float_t         N2_particle1_VtxZ;
   Float_t         N2_particle2_VtxZ;
   Float_t         N2_particle3_VtxZ;
   Int_t           C1_particle0_Flavor;
   Int_t           C1_particle1_Flavor;
   Int_t           C1_particle2_Flavor;
   Int_t           C1_particle3_Flavor;
   Float_t         C1_particle0_Eta;
   Float_t         C1_particle1_Eta;
   Float_t         C1_particle2_Eta;
   Float_t         C1_particle3_Eta;
   Float_t         C1_particle0_Phi;
   Float_t         C1_particle1_Phi;
   Float_t         C1_particle2_Phi;
   Float_t         C1_particle3_Phi;
   Float_t         C1_particle0_pT;
   Float_t         C1_particle1_pT;
   Float_t         C1_particle2_pT;
   Float_t         C1_particle3_pT;
   Float_t         C1_particle0_VtxX;
   Float_t         C1_particle1_VtxX;
   Float_t         C1_particle2_VtxX;
   Float_t         C1_particle3_VtxX;
   Float_t         C1_particle0_VtxY;
   Float_t         C1_particle1_VtxY;
   Float_t         C1_particle2_VtxY;
   Float_t         C1_particle3_VtxY;
   Float_t         C1_particle0_VtxZ;
   Float_t         C1_particle1_VtxZ;
   Float_t         C1_particle2_VtxZ;
   Float_t         C1_particle3_VtxZ;
   std::vector<float>   *TrackEta;
   std::vector<float>   *TrackPhi;
   std::vector<float>   *TrackPt;
   std::vector<int>     *TrackCharge;
   std::vector<int>     *TrackOriginFlavor;
   std::vector<int>     *TrackFlavor;
   std::vector<float>   *TrackD0;
   std::vector<float>   *TrackZ0;
   std::vector<float>   *TrackZ0OriginV;
   std::vector<float>   *TrackOriginVz;
   std::vector<float>   *TrackZ0_CVz;
   std::vector<float>   *TrackD0Err;
   std::vector<float>   *TrackZ0Err;
   std::vector<float>   *TrackPtcone20;
   std::vector<float>   *TrackPtcone30;
   std::vector<float>   *TrackPtcone40;
   std::vector<float>   *etcone20Topo;
   std::vector<float>   *etclus40Topo;
   std::vector<int>     *nIBLHits;
   std::vector<int>     *nPixLayers;
   std::vector<int>     *nExpBLayerHits;
   std::vector<int>     *nPixHits;
   std::vector<int>     *nPixHoles;
   std::vector<int>     *nPixOutliers;
   std::vector<int>     *nSCTHits;
   std::vector<int>     *nSCTHoles;
   std::vector<int>     *nSCTSharedHits;
   std::vector<int>     *nSCTOutliers;
   std::vector<int>     *nTRTHits;
   std::vector<int>     *nPixSpoiltHits;
   std::vector<int>     *nGangedFlaggedFakes;
   std::vector<float>   *eProbabilityComb;
   std::vector<float>   *eProbabilityHT;
   std::vector<float>   *pixeldEdx;
   Int_t           nJet30;
   Int_t           nJet25;
   Int_t           nJet20;
   Int_t           nTotalJet;
   Int_t           nTotalJet20;
   Int_t           nBJet20_MV2c10;
   Int_t           nBJet30_MV2c10;
   Int_t           nBJet20_MV2c10_FixedCutBEff_60;
   Int_t           nBJet20_MV2c10_FixedCutBEff_70;
   Int_t           nBJet20_MV2c10_FixedCutBEff_77;
   Int_t           nBJet20_MV2c10_FixedCutBEff_85;
   Int_t           nBJet30_MV2c10_FixedCutBEff_60;
   Int_t           nBJet30_MV2c10_FixedCutBEff_70;
   Int_t           nBJet30_MV2c10_FixedCutBEff_77;
   Int_t           nBJet30_MV2c10_FixedCutBEff_85;
   Float_t         jet1Pt;
   Float_t         jet1Phi;
   std::vector<float>   *jetPt;
   std::vector<float>   *jetEta;
   std::vector<float>   *jetPhi;
   std::vector<float>   *jetM;
   std::vector<int>   *jetnTrk;
   std::vector<float>   *jetTileEnergy;
   std::vector<float>   *ajetPt;
   std::vector<float>   *ajetEta;
   std::vector<float>   *ajetPhi;
   std::vector<float>   *ajetM;
   std::vector<int>   *ajetnTrk;
   std::vector<float>   *ajetTileEnergy;
   Float_t         met_Et;
   Float_t         met_px;
   Float_t         met_py;
   Float_t         met_Phi;
   Float_t         met_pz;
   Float_t         met_Signif;
   Float_t         mt_lep1;
   Float_t         METOverHT;
   Float_t         DPhiJ1Met;
   Float_t         minDPhiAllJetsMet;
   Float_t         METOverHTLep;
   Float_t         mll;
   Float_t         Rll;
   Float_t         MTauTau;
   Float_t         mt2leplsp_100;
   Double_t        pileupWeight;
   Double_t        leptonWeight;
   Double_t        eventWeight;
   Double_t        genWeight;
   Double_t        bTagWeight;
   Double_t        jvtWeight;
   Double_t        genWeightUp;
   Double_t        genWeightDown;
   Double_t        truthMll;
   Double_t        winoBinoMllWeight;
   Double_t        FFWeight;
   Int_t           nLep_antiID;
   Bool_t          lep1AntiID;
   Bool_t          lep2AntiID;
   Int_t           nLep_signalActual;
   Bool_t          lep1SignalActual;
   Bool_t          lep2SignalActual;
   Double_t        ETMissTrigSF;
   Double_t        RJR_RISR;
   Double_t        RJR_MS;
   Double_t        RJR_dphiISRI;
   Double_t        SUSYPt;
   Double_t        ISRWeight;
   ULong64_t       PRWHash;
   ULong64_t       EventNumber;
   Float_t         xsec;
   Float_t         GenHt;
   Float_t         GenMET;
   Int_t           DatasetNumber;
   Int_t           RunNumber;
   Int_t           RandomRunNumber;
   Int_t           FS;

   // List of branches
   TBranch        *b_trigWeight_metTrig;   //!
   TBranch        *b_trigMatch_metTrig;   //!
   TBranch        *b_trigWeight_singleMuonTrig;   //!
   TBranch        *b_trigMatch_singleMuonTrig;   //!
   TBranch        *b_trigWeight_singleElectronTrig;   //!
   TBranch        *b_trigMatch_singleElectronTrig;   //!
   TBranch        *b_nLep_base;   //!
   TBranch        *b_nLep_signal;   //!
   TBranch        *b_nEle_signal;   //!
   TBranch        *b_nMu_signal;   //!
   TBranch        *b_nLep_signal_PLVLooseLoose;   //!
   TBranch        *b_nLep_signal_PLVLooseTight;   //!
   TBranch        *b_nLep_signal_PLVTightLoose;   //!
   TBranch        *b_nLep_signal_PLVTightTight;   //!
   TBranch        *b_lep1Flavor;   //!
   TBranch        *b_lep1OriginFlavor;   //!
   TBranch        *b_lep1Charge;   //!
   TBranch        *b_lep1Author;   //!
   TBranch        *b_lep1Pt;   //!
   TBranch        *b_lep1Eta;   //!
   TBranch        *b_lep1Phi;   //!
   TBranch        *b_lep1M;   //!
   TBranch        *b_lep1D0;   //!
   TBranch        *b_lep1Z0;   //!
   TBranch        *b_lep1D0Sig;   //!
   TBranch        *b_lep1Z0SinTheta;   //!
   TBranch        *b_lep1Z0OriginV;   //!
   TBranch        *b_lep1OriginVz;   //!
   TBranch        *b_lep1Ptcone20;   //!
   TBranch        *b_lep1Ptcone30;   //!
   TBranch        *b_lep1Topoetcone20;   //!
   TBranch        *b_lep1Topoetcone30;   //!
   TBranch        *b_lep1Ptvarcone20;   //!
   TBranch        *b_lep1Ptvarcone30;   //!
   TBranch        *b_lep1PassOR;   //!
   TBranch        *b_lep1Type;   //!
   TBranch        *b_lep1Origin;   //!
   TBranch        *b_lep1PassBL;   //!
   TBranch        *b_lep1VeryLoose;   //!
   TBranch        *b_lep1Loose;   //!
   TBranch        *b_lep1Medium;   //!
   TBranch        *b_lep1Tight;   //!
   TBranch        *b_lep1LowPtWP;   //!
   TBranch        *b_lep1minDRAllJetsLepton;   //!
   TBranch        *b_lep1IsoCorrLoose;   //!
   TBranch        *b_lep1IsoCorrTight;   //!
   TBranch        *b_lep1IsoCorrGradient;   //!
   TBranch        *b_lep1IsoCorrGradientLoose;   //!
   TBranch        *b_lep1IsoCorrLooseTrackOnly;   //!
   TBranch        *b_lep1IsoCorrFixedCutLoose;   //!
   TBranch        *b_lep1IsoCorrFixedCutTight;   //!
   TBranch        *b_lep1IsoCorrFixedCutTightTrackOnly;   //!
   TBranch        *b_lep1IsoCorrFCTightTrackOnly;   //!
   TBranch        *b_lep1IsoCorrPLVLoose;   //!
   TBranch        *b_lep1IsoCorrPLVTight;   //!
   TBranch        *b_lep1Signal;   //!
   TBranch        *b_lep1SignalPLVLoose;   //!
   TBranch        *b_lep1SignalPLVTight;   //!
   TBranch        *b_lep1TruthMatched;   //!
   TBranch        *b_lep1PromptLepVeto_score;   //!
   TBranch        *b_lep1LowPtPLV_score;   //!
   TBranch        *b_lep1PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep1PLTInput_DRlj;   //!
   TBranch        *b_lep1PLTInput_rnnip;   //!
   TBranch        *b_lep1PLTInput_DL1mu;   //!
   TBranch        *b_lep1PLTInput_PtRel;   //!
   TBranch        *b_lep1PLTInput_PtFrac;   //!
   TBranch        *b_lep1PLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep1PLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep1CorrPromptLepVeto_score;   //!
   TBranch        *b_lep1CorrLowPtPLV_score;   //!
   TBranch        *b_lep1CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep1CorrPLTInput_DRlj;   //!
   TBranch        *b_lep1CorrPLTInput_rnnip;   //!
   TBranch        *b_lep1CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep1CorrPLTInput_PtRel;   //!
   TBranch        *b_lep1CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep1CorrPLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep1CorrPLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep2Flavor;   //!
   TBranch        *b_lep2OriginFlavor;   //!
   TBranch        *b_lep2Charge;   //!
   TBranch        *b_lep2Author;   //!
   TBranch        *b_lep2Pt;   //!
   TBranch        *b_lep2Eta;   //!
   TBranch        *b_lep2Phi;   //!
   TBranch        *b_lep2M;   //!
   TBranch        *b_lep2D0;   //!
   TBranch        *b_lep2Z0;   //!
   TBranch        *b_lep2D0Sig;   //!
   TBranch        *b_lep2Z0SinTheta;   //!
   TBranch        *b_lep2Z0OriginV;   //!
   TBranch        *b_lep2OriginVz;   //!
   TBranch        *b_lep2Ptcone20;   //!
   TBranch        *b_lep2Ptcone30;   //!
   TBranch        *b_lep2Topoetcone20;   //!
   TBranch        *b_lep2Topoetcone30;   //!
   TBranch        *b_lep2Ptvarcone20;   //!
   TBranch        *b_lep2Ptvarcone30;   //!
   TBranch        *b_lep2PassOR;   //!
   TBranch        *b_lep2Type;   //!
   TBranch        *b_lep2Origin;   //!
   TBranch        *b_lep2PassBL;   //!
   TBranch        *b_lep2VeryLoose;   //!
   TBranch        *b_lep2Loose;   //!
   TBranch        *b_lep2Medium;   //!
   TBranch        *b_lep2Tight;   //!
   TBranch        *b_lep2LowPtWP;   //!
   TBranch        *b_lep2minDRAllJetsLepton;   //!
   TBranch        *b_lep2IsoCorrLoose;   //!
   TBranch        *b_lep2IsoCorrTight;   //!
   TBranch        *b_lep2IsoCorrGradient;   //!
   TBranch        *b_lep2IsoCorrGradientLoose;   //!
   TBranch        *b_lep2IsoCorrLooseTrackOnly;   //!
   TBranch        *b_lep2IsoCorrFixedCutLoose;   //!
   TBranch        *b_lep2IsoCorrFixedCutTight;   //!
   TBranch        *b_lep2IsoCorrFixedCutTightTrackOnly;   //!
   TBranch        *b_lep2IsoCorrFCTightTrackOnly;   //!
   TBranch        *b_lep2IsoCorrPLVLoose;   //!
   TBranch        *b_lep2IsoCorrPLVTight;   //!
   TBranch        *b_lep2Signal;   //!
   TBranch        *b_lep2SignalPLVLoose;   //!
   TBranch        *b_lep2SignalPLVTight;   //!
   TBranch        *b_lep2TruthMatched;   //!
   TBranch        *b_lep2PromptLepVeto_score;   //!
   TBranch        *b_lep2LowPtPLV_score;   //!
   TBranch        *b_lep2PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep2PLTInput_DRlj;   //!
   TBranch        *b_lep2PLTInput_rnnip;   //!
   TBranch        *b_lep2PLTInput_DL1mu;   //!
   TBranch        *b_lep2PLTInput_PtRel;   //!
   TBranch        *b_lep2PLTInput_PtFrac;   //!
   TBranch        *b_lep2PLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep2PLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep2CorrPromptLepVeto_score;   //!
   TBranch        *b_lep2CorrLowPtPLV_score;   //!
   TBranch        *b_lep2CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep2CorrPLTInput_DRlj;   //!
   TBranch        *b_lep2CorrPLTInput_rnnip;   //!
   TBranch        *b_lep2CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep2CorrPLTInput_PtRel;   //!
   TBranch        *b_lep2CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep2CorrPLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep2CorrPLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep3Flavor;   //!
   TBranch        *b_lep3OriginFlavor;   //!
   TBranch        *b_lep3Charge;   //!
   TBranch        *b_lep3Author;   //!
   TBranch        *b_lep3Pt;   //!
   TBranch        *b_lep3Eta;   //!
   TBranch        *b_lep3Phi;   //!
   TBranch        *b_lep3M;   //!
   TBranch        *b_lep3D0;   //!
   TBranch        *b_lep3Z0;   //!
   TBranch        *b_lep3D0Sig;   //!
   TBranch        *b_lep3Z0SinTheta;   //!
   TBranch        *b_lep3Z0OriginV;   //!
   TBranch        *b_lep3OriginVz;   //!
   TBranch        *b_lep3Ptcone20;   //!
   TBranch        *b_lep3Ptcone30;   //!
   TBranch        *b_lep3Topoetcone20;   //!
   TBranch        *b_lep3Topoetcone30;   //!
   TBranch        *b_lep3Ptvarcone20;   //!
   TBranch        *b_lep3Ptvarcone30;   //!
   TBranch        *b_lep3PassOR;   //!
   TBranch        *b_lep3Type;   //!
   TBranch        *b_lep3Origin;   //!
   TBranch        *b_lep3PassBL;   //!
   TBranch        *b_lep3VeryLoose;   //!
   TBranch        *b_lep3Loose;   //!
   TBranch        *b_lep3Medium;   //!
   TBranch        *b_lep3Tight;   //!
   TBranch        *b_lep3LowPtWP;   //!
   TBranch        *b_lep3minDRAllJetsLepton;   //!
   TBranch        *b_lep3IsoCorrLoose;   //!
   TBranch        *b_lep3IsoCorrTight;   //!
   TBranch        *b_lep3IsoCorrGradient;   //!
   TBranch        *b_lep3IsoCorrGradientLoose;   //!
   TBranch        *b_lep3IsoCorrLooseTrackOnly;   //!
   TBranch        *b_lep3IsoCorrFixedCutLoose;   //!
   TBranch        *b_lep3IsoCorrFixedCutTight;   //!
   TBranch        *b_lep3IsoCorrFixedCutTightTrackOnly;   //!
   TBranch        *b_lep3IsoCorrFCTightTrackOnly;   //!
   TBranch        *b_lep3IsoCorrPLVLoose;   //!
   TBranch        *b_lep3IsoCorrPLVTight;   //!
   TBranch        *b_lep3Signal;   //!
   TBranch        *b_lep3SignalPLVLoose;   //!
   TBranch        *b_lep3SignalPLVTight;   //!
   TBranch        *b_lep3TruthMatched;   //!
   TBranch        *b_lep3PromptLepVeto_score;   //!
   TBranch        *b_lep3LowPtPLV_score;   //!
   TBranch        *b_lep3PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep3PLTInput_DRlj;   //!
   TBranch        *b_lep3PLTInput_rnnip;   //!
   TBranch        *b_lep3PLTInput_DL1mu;   //!
   TBranch        *b_lep3PLTInput_PtRel;   //!
   TBranch        *b_lep3PLTInput_PtFrac;   //!
   TBranch        *b_lep3PLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep3PLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep3CorrPromptLepVeto_score;   //!
   TBranch        *b_lep3CorrLowPtPLV_score;   //!
   TBranch        *b_lep3CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep3CorrPLTInput_DRlj;   //!
   TBranch        *b_lep3CorrPLTInput_rnnip;   //!
   TBranch        *b_lep3CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep3CorrPLTInput_PtRel;   //!
   TBranch        *b_lep3CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep3CorrPLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep3CorrPLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep4Flavor;   //!
   TBranch        *b_lep4OriginFlavor;   //!
   TBranch        *b_lep4Charge;   //!
   TBranch        *b_lep4Author;   //!
   TBranch        *b_lep4Pt;   //!
   TBranch        *b_lep4Eta;   //!
   TBranch        *b_lep4Phi;   //!
   TBranch        *b_lep4M;   //!
   TBranch        *b_lep4D0;   //!
   TBranch        *b_lep4Z0;   //!
   TBranch        *b_lep4D0Sig;   //!
   TBranch        *b_lep4Z0SinTheta;   //!
   TBranch        *b_lep4Z0OriginV;   //!
   TBranch        *b_lep4OriginVz;   //!
   TBranch        *b_lep4Ptcone20;   //!
   TBranch        *b_lep4Ptcone30;   //!
   TBranch        *b_lep4Topoetcone20;   //!
   TBranch        *b_lep4Topoetcone30;   //!
   TBranch        *b_lep4Ptvarcone20;   //!
   TBranch        *b_lep4Ptvarcone30;   //!
   TBranch        *b_lep4PassOR;   //!
   TBranch        *b_lep4Type;   //!
   TBranch        *b_lep4Origin;   //!
   TBranch        *b_lep4PassBL;   //!
   TBranch        *b_lep4VeryLoose;   //!
   TBranch        *b_lep4Loose;   //!
   TBranch        *b_lep4Medium;   //!
   TBranch        *b_lep4Tight;   //!
   TBranch        *b_lep4LowPtWP;   //!
   TBranch        *b_lep4minDRAllJetsLepton;   //!
   TBranch        *b_lep4IsoCorrLoose;   //!
   TBranch        *b_lep4IsoCorrTight;   //!
   TBranch        *b_lep4IsoCorrGradient;   //!
   TBranch        *b_lep4IsoCorrGradientLoose;   //!
   TBranch        *b_lep4IsoCorrLooseTrackOnly;   //!
   TBranch        *b_lep4IsoCorrFixedCutLoose;   //!
   TBranch        *b_lep4IsoCorrFixedCutTight;   //!
   TBranch        *b_lep4IsoCorrFixedCutTightTrackOnly;   //!
   TBranch        *b_lep4IsoCorrFCTightTrackOnly;   //!
   TBranch        *b_lep4IsoCorrPLVLoose;   //!
   TBranch        *b_lep4IsoCorrPLVTight;   //!
   TBranch        *b_lep4Signal;   //!
   TBranch        *b_lep4SignalPLVLoose;   //!
   TBranch        *b_lep4SignalPLVTight;   //!
   TBranch        *b_lep4TruthMatched;   //!
   TBranch        *b_lep4PromptLepVeto_score;   //!
   TBranch        *b_lep4LowPtPLV_score;   //!
   TBranch        *b_lep4PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep4PLTInput_DRlj;   //!
   TBranch        *b_lep4PLTInput_rnnip;   //!
   TBranch        *b_lep4PLTInput_DL1mu;   //!
   TBranch        *b_lep4PLTInput_PtRel;   //!
   TBranch        *b_lep4PLTInput_PtFrac;   //!
   TBranch        *b_lep4PLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep4PLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_lep4CorrPromptLepVeto_score;   //!
   TBranch        *b_lep4CorrLowPtPLV_score;   //!
   TBranch        *b_lep4CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep4CorrPLTInput_DRlj;   //!
   TBranch        *b_lep4CorrPLTInput_rnnip;   //!
   TBranch        *b_lep4CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep4CorrPLTInput_PtRel;   //!
   TBranch        *b_lep4CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep4CorrPLTInput_Topoetcone30Rel;   //!
   TBranch        *b_lep4CorrPLTInput_Ptvarcone30Rel;   //!
   TBranch        *b_N2_particle0_Flavor;   //!
   TBranch        *b_N2_particle1_Flavor;   //!
   TBranch        *b_N2_particle2_Flavor;   //!
   TBranch        *b_N2_particle3_Flavor;   //!
   TBranch        *b_N2_particle0_Eta;   //!
   TBranch        *b_N2_particle1_Eta;   //!
   TBranch        *b_N2_particle2_Eta;   //!
   TBranch        *b_N2_particle3_Eta;   //!
   TBranch        *b_N2_particle0_Phi;   //!
   TBranch        *b_N2_particle1_Phi;   //!
   TBranch        *b_N2_particle2_Phi;   //!
   TBranch        *b_N2_particle3_Phi;   //!
   TBranch        *b_N2_particle0_pT;   //!
   TBranch        *b_N2_particle1_pT;   //!
   TBranch        *b_N2_particle2_pT;   //!
   TBranch        *b_N2_particle3_pT;   //!
   TBranch        *b_N2_particle0_VtxX;   //!
   TBranch        *b_N2_particle1_VtxX;   //!
   TBranch        *b_N2_particle2_VtxX;   //!
   TBranch        *b_N2_particle3_VtxX;   //!
   TBranch        *b_N2_particle0_VtxY;   //!
   TBranch        *b_N2_particle1_VtxY;   //!
   TBranch        *b_N2_particle2_VtxY;   //!
   TBranch        *b_N2_particle3_VtxY;   //!
   TBranch        *b_N2_particle0_VtxZ;   //!
   TBranch        *b_N2_particle1_VtxZ;   //!
   TBranch        *b_N2_particle2_VtxZ;   //!
   TBranch        *b_N2_particle3_VtxZ;   //!
   TBranch        *b_C1_particle0_Flavor;   //!
   TBranch        *b_C1_particle1_Flavor;   //!
   TBranch        *b_C1_particle2_Flavor;   //!
   TBranch        *b_C1_particle3_Flavor;   //!
   TBranch        *b_C1_particle0_Eta;   //!
   TBranch        *b_C1_particle1_Eta;   //!
   TBranch        *b_C1_particle2_Eta;   //!
   TBranch        *b_C1_particle3_Eta;   //!
   TBranch        *b_C1_particle0_Phi;   //!
   TBranch        *b_C1_particle1_Phi;   //!
   TBranch        *b_C1_particle2_Phi;   //!
   TBranch        *b_C1_particle3_Phi;   //!
   TBranch        *b_C1_particle0_pT;   //!
   TBranch        *b_C1_particle1_pT;   //!
   TBranch        *b_C1_particle2_pT;   //!
   TBranch        *b_C1_particle3_pT;   //!
   TBranch        *b_C1_particle0_VtxX;   //!
   TBranch        *b_C1_particle1_VtxX;   //!
   TBranch        *b_C1_particle2_VtxX;   //!
   TBranch        *b_C1_particle3_VtxX;   //!
   TBranch        *b_C1_particle0_VtxY;   //!
   TBranch        *b_C1_particle1_VtxY;   //!
   TBranch        *b_C1_particle2_VtxY;   //!
   TBranch        *b_C1_particle3_VtxY;   //!
   TBranch        *b_C1_particle0_VtxZ;   //!
   TBranch        *b_C1_particle1_VtxZ;   //!
   TBranch        *b_C1_particle2_VtxZ;   //!
   TBranch        *b_C1_particle3_VtxZ;   //!
   TBranch        *b_TrackEta;   //!
   TBranch        *b_TrackPhi;   //!
   TBranch        *b_TrackPt;   //!
   TBranch        *b_TrackCharge;   //!
   TBranch        *b_TrackOriginFlavor;   //!
   TBranch        *b_TrackFlavor;   //!
   TBranch        *b_TrackD0;   //!
   TBranch        *b_TrackZ0;   //!
   TBranch        *b_TrackZ0OriginV;   //!
   TBranch        *b_TrackOriginVz;   //!
   TBranch        *b_TrackZ0_CVz;   //!
   TBranch        *b_TrackD0Err;   //!
   TBranch        *b_TrackZ0Err;   //!
   TBranch        *b_TrackPtcone20;   //!
   TBranch        *b_TrackPtcone30;   //!
   TBranch        *b_TrackPtcone40;   //!
   TBranch        *b_etcone20Topo;   //!
   TBranch        *b_etclus40Topo;   //!
   TBranch        *b_nIBLHits;   //!
   TBranch        *b_nPixLayers;   //!
   TBranch        *b_nExpBLayerHits;   //!
   TBranch        *b_nPixHits;   //!
   TBranch        *b_nPixHoles;   //!
   TBranch        *b_nPixOutliers;   //!
   TBranch        *b_nSCTHits;   //!
   TBranch        *b_nSCTHoles;   //!
   TBranch        *b_nSCTSharedHits;   //!
   TBranch        *b_nSCTOutliers;   //!
   TBranch        *b_nTRTHits;   //!
   TBranch        *b_nPixSpoiltHits;   //!
   TBranch        *b_nGangedFlaggedFakes;   //!
   TBranch        *b_eProbabilityComb;   //!
   TBranch        *b_eProbabilityHT;   //!
   TBranch        *b_pixeldEdx;   //!
   TBranch        *b_nJet30;   //!
   TBranch        *b_nJet25;   //!
   TBranch        *b_nJet20;   //!
   TBranch        *b_nTotalJet;   //!
   TBranch        *b_nTotalJet20;   //!
   TBranch        *b_nBJet20_MV2c10;   //!
   TBranch        *b_nBJet30_MV2c10;   //!
   TBranch        *b_nBJet20_MV2c10_FixedCutBEff_60;   //!
   TBranch        *b_nBJet20_MV2c10_FixedCutBEff_70;   //!
   TBranch        *b_nBJet20_MV2c10_FixedCutBEff_77;   //!
   TBranch        *b_nBJet20_MV2c10_FixedCutBEff_85;   //!
   TBranch        *b_nBJet30_MV2c10_FixedCutBEff_60;   //!
   TBranch        *b_nBJet30_MV2c10_FixedCutBEff_70;   //!
   TBranch        *b_nBJet30_MV2c10_FixedCutBEff_77;   //!
   TBranch        *b_nBJet30_MV2c10_FixedCutBEff_85;   //!
   TBranch        *b_jet1Pt;   //!
   TBranch        *b_jet1Phi;   //!
   TBranch        *b_jetPt;   //!
   TBranch        *b_jetEta;   //!
   TBranch        *b_jetPhi;   //!
   TBranch        *b_jetM;   //!
   TBranch        *b_jetnTrk;   //!
   TBranch        *b_jetTileEnergy;   //!
   TBranch        *b_ajetPt;   //!
   TBranch        *b_ajetEta;   //!
   TBranch        *b_ajetPhi;   //!
   TBranch        *b_ajetM;   //!
   TBranch        *b_ajetnTrk;   //!
   TBranch        *b_ajetTileEnergy;   //!
   TBranch        *b_met_Et;   //!
   TBranch        *b_met_px;   //!
   TBranch        *b_met_py;   //!
   TBranch        *b_met_Phi;   //!
   TBranch        *b_met_pz;   //!
   TBranch        *b_met_Signif;   //!
   TBranch        *b_mt_lep1;   //!
   TBranch        *b_METOverHT;   //!
   TBranch        *b_DPhiJ1Met;   //!
   TBranch        *b_minDPhiAllJetsMet;   //!
   TBranch        *b_METOverHTLep;   //!
   TBranch        *b_mll;   //!
   TBranch        *b_Rll;   //!
   TBranch        *b_MTauTau;   //!
   TBranch        *b_mt2leplsp_100;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_leptonWeight;   //!
   TBranch        *b_eventWeight;   //!
   TBranch        *b_genWeight;   //!
   TBranch        *b_bTagWeight;   //!
   TBranch        *b_jvtWeight;   //!
   TBranch        *b_genWeightUp;   //!
   TBranch        *b_genWeightDown;   //!
   TBranch        *b_truthMll;   //!
   TBranch        *b_winoBinoMllWeight;   //!
   TBranch        *b_FFWeight;   //!
   TBranch        *b_nLep_antiID;   //!
   TBranch        *b_lep1AntiID;   //!
   TBranch        *b_lep2AntiID;   //!
   TBranch        *b_nLep_signalActual;   //!
   TBranch        *b_lep1SignalActual;   //!
   TBranch        *b_lep2SignalActual;   //!
   TBranch        *b_ETMissTrigSF;   //!
   TBranch        *b_RJR_RISR;   //!
   TBranch        *b_RJR_MS;   //!
   TBranch        *b_RJR_dphiISRI;   //!
   TBranch        *b_SUSYPt;   //!
   TBranch        *b_ISRWeight;   //!
   TBranch        *b_PRWHash;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_GenHt;   //!
   TBranch        *b_GenMET;   //!
   TBranch        *b_DatasetNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_FS;   //!

   tree_NoSys(TTree *tree=0);
   virtual ~tree_NoSys();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TH1F *WeightHist, std::string FILE, double MassCUT,std::string CombiSign, std::string MCorDATA);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);


   CP::IsolationLowPtPLVTool* isoLowPtPLVTool;

};

#endif

#ifdef tree_NoSys_cxx
tree_NoSys::tree_NoSys(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("SUSY.C1pN2.100p0_99p0.SUSY19.01.r10201.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("SUSY.C1pN2.100p0_99p0.SUSY19.01.r10201.root");
      }
      f->GetObject("tree_NoSys",tree);

   }
   Init(tree);
}

tree_NoSys::~tree_NoSys()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tree_NoSys::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tree_NoSys::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tree_NoSys::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   TrackEta = 0;
   TrackPhi = 0;
   TrackPt = 0;
   TrackCharge = 0;
   TrackOriginFlavor = 0;
   TrackFlavor = 0;
   TrackD0 = 0;
   TrackZ0 = 0;
   TrackZ0OriginV = 0;
   TrackOriginVz = 0;
   TrackZ0_CVz = 0;
   TrackD0Err = 0;
   TrackZ0Err = 0;
   TrackPtcone20 = 0;
   TrackPtcone30 = 0;
   TrackPtcone40 = 0;
   etcone20Topo = 0;
   etclus40Topo = 0;
   nIBLHits = 0;
   nPixLayers = 0;
   nExpBLayerHits = 0;
   nPixHits = 0;
   nPixHoles = 0;
   nPixOutliers = 0;
   nSCTHits = 0;
   nSCTHoles = 0;
   nSCTSharedHits = 0;
   nSCTOutliers = 0;
   nTRTHits = 0;
   nPixSpoiltHits = 0;
   nGangedFlaggedFakes = 0;
   eProbabilityComb = 0;
   eProbabilityHT = 0;
   pixeldEdx = 0;
   jetPt = 0;
   jetEta = 0;
   jetPhi = 0;
   jetM = 0;
   jetnTrk = 0;
   jetTileEnergy = 0;
   ajetPt = 0;
   ajetEta = 0;
   ajetPhi = 0;
   ajetM = 0;
   ajetnTrk = 0;
   ajetTileEnergy = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig, &b_trigWeight_metTrig);
   fChain->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig, &b_trigMatch_metTrig);
   fChain->SetBranchAddress("trigWeight_singleMuonTrig", &trigWeight_singleMuonTrig, &b_trigWeight_singleMuonTrig);
   fChain->SetBranchAddress("trigMatch_singleMuonTrig", &trigMatch_singleMuonTrig, &b_trigMatch_singleMuonTrig);
   fChain->SetBranchAddress("trigWeight_singleElectronTrig", &trigWeight_singleElectronTrig, &b_trigWeight_singleElectronTrig);
   fChain->SetBranchAddress("trigMatch_singleElectronTrig", &trigMatch_singleElectronTrig, &b_trigMatch_singleElectronTrig);
   fChain->SetBranchAddress("nLep_base", &nLep_base, &b_nLep_base);
   fChain->SetBranchAddress("nLep_signal", &nLep_signal, &b_nLep_signal);
   fChain->SetBranchAddress("nEle_signal", &nEle_signal, &b_nEle_signal);
   fChain->SetBranchAddress("nMu_signal", &nMu_signal, &b_nMu_signal);
   fChain->SetBranchAddress("nLep_signal_PLVLooseLoose", &nLep_signal_PLVLooseLoose, &b_nLep_signal_PLVLooseLoose);
   fChain->SetBranchAddress("nLep_signal_PLVLooseTight", &nLep_signal_PLVLooseTight, &b_nLep_signal_PLVLooseTight);
   fChain->SetBranchAddress("nLep_signal_PLVTightLoose", &nLep_signal_PLVTightLoose, &b_nLep_signal_PLVTightLoose);
   fChain->SetBranchAddress("nLep_signal_PLVTightTight", &nLep_signal_PLVTightTight, &b_nLep_signal_PLVTightTight);
   fChain->SetBranchAddress("lep1Flavor", &lep1Flavor, &b_lep1Flavor);
   fChain->SetBranchAddress("lep1OriginFlavor", &lep1OriginFlavor, &b_lep1OriginFlavor);
   fChain->SetBranchAddress("lep1Charge", &lep1Charge, &b_lep1Charge);
   fChain->SetBranchAddress("lep1Author", &lep1Author, &b_lep1Author);
   fChain->SetBranchAddress("lep1Pt", &lep1Pt, &b_lep1Pt);
   fChain->SetBranchAddress("lep1Eta", &lep1Eta, &b_lep1Eta);
   fChain->SetBranchAddress("lep1Phi", &lep1Phi, &b_lep1Phi);
   fChain->SetBranchAddress("lep1M", &lep1M, &b_lep1M);
   fChain->SetBranchAddress("lep1D0", &lep1D0, &b_lep1D0);
   fChain->SetBranchAddress("lep1Z0", &lep1Z0, &b_lep1Z0);
   fChain->SetBranchAddress("lep1D0Sig", &lep1D0Sig, &b_lep1D0Sig);
   fChain->SetBranchAddress("lep1Z0SinTheta", &lep1Z0SinTheta, &b_lep1Z0SinTheta);
   fChain->SetBranchAddress("lep1Z0OriginV", &lep1Z0OriginV, &b_lep1Z0OriginV);
   fChain->SetBranchAddress("lep1OriginVz", &lep1OriginVz, &b_lep1OriginVz);
   fChain->SetBranchAddress("lep1Ptcone20", &lep1Ptcone20, &b_lep1Ptcone20);
   fChain->SetBranchAddress("lep1Ptcone30", &lep1Ptcone30, &b_lep1Ptcone30);
   fChain->SetBranchAddress("lep1Topoetcone20", &lep1Topoetcone20, &b_lep1Topoetcone20);
   fChain->SetBranchAddress("lep1Topoetcone30", &lep1Topoetcone30, &b_lep1Topoetcone30);
   fChain->SetBranchAddress("lep1Ptvarcone20", &lep1Ptvarcone20, &b_lep1Ptvarcone20);
   fChain->SetBranchAddress("lep1Ptvarcone30", &lep1Ptvarcone30, &b_lep1Ptvarcone30);
   fChain->SetBranchAddress("lep1PassOR", &lep1PassOR, &b_lep1PassOR);
   fChain->SetBranchAddress("lep1Type", &lep1Type, &b_lep1Type);
   fChain->SetBranchAddress("lep1Origin", &lep1Origin, &b_lep1Origin);
   fChain->SetBranchAddress("lep1PassBL", &lep1PassBL, &b_lep1PassBL);
   fChain->SetBranchAddress("lep1VeryLoose", &lep1VeryLoose, &b_lep1VeryLoose);
   fChain->SetBranchAddress("lep1Loose", &lep1Loose, &b_lep1Loose);
   fChain->SetBranchAddress("lep1Medium", &lep1Medium, &b_lep1Medium);
   fChain->SetBranchAddress("lep1Tight", &lep1Tight, &b_lep1Tight);
   fChain->SetBranchAddress("lep1LowPtWP", &lep1LowPtWP, &b_lep1LowPtWP);
   fChain->SetBranchAddress("lep1minDRAllJetsLepton", &lep1minDRAllJetsLepton, &b_lep1minDRAllJetsLepton);
   fChain->SetBranchAddress("lep1IsoCorrLoose", &lep1IsoCorrLoose, &b_lep1IsoCorrLoose);
   fChain->SetBranchAddress("lep1IsoCorrTight", &lep1IsoCorrTight, &b_lep1IsoCorrTight);
   fChain->SetBranchAddress("lep1IsoCorrGradient", &lep1IsoCorrGradient, &b_lep1IsoCorrGradient);
   fChain->SetBranchAddress("lep1IsoCorrGradientLoose", &lep1IsoCorrGradientLoose, &b_lep1IsoCorrGradientLoose);
   fChain->SetBranchAddress("lep1IsoCorrLooseTrackOnly", &lep1IsoCorrLooseTrackOnly, &b_lep1IsoCorrLooseTrackOnly);
   fChain->SetBranchAddress("lep1IsoCorrFixedCutLoose", &lep1IsoCorrFixedCutLoose, &b_lep1IsoCorrFixedCutLoose);
   fChain->SetBranchAddress("lep1IsoCorrFixedCutTight", &lep1IsoCorrFixedCutTight, &b_lep1IsoCorrFixedCutTight);
   fChain->SetBranchAddress("lep1IsoCorrFixedCutTightTrackOnly", &lep1IsoCorrFixedCutTightTrackOnly, &b_lep1IsoCorrFixedCutTightTrackOnly);
   fChain->SetBranchAddress("lep1IsoCorrFCTightTrackOnly", &lep1IsoCorrFCTightTrackOnly, &b_lep1IsoCorrFCTightTrackOnly);
   fChain->SetBranchAddress("lep1IsoCorrPLVLoose", &lep1IsoCorrPLVLoose, &b_lep1IsoCorrPLVLoose);
   fChain->SetBranchAddress("lep1IsoCorrPLVTight", &lep1IsoCorrPLVTight, &b_lep1IsoCorrPLVTight);
   fChain->SetBranchAddress("lep1Signal", &lep1Signal, &b_lep1Signal);
   fChain->SetBranchAddress("lep1SignalPLVLoose", &lep1SignalPLVLoose, &b_lep1SignalPLVLoose);
   fChain->SetBranchAddress("lep1SignalPLVTight", &lep1SignalPLVTight, &b_lep1SignalPLVTight);
   fChain->SetBranchAddress("lep1TruthMatched", &lep1TruthMatched, &b_lep1TruthMatched);
   fChain->SetBranchAddress("lep1PromptLepVeto_score", &lep1PromptLepVeto_score, &b_lep1PromptLepVeto_score);
   fChain->SetBranchAddress("lep1LowPtPLV_score", &lep1LowPtPLV_score, &b_lep1LowPtPLV_score);
   fChain->SetBranchAddress("lep1PLTInput_TrackJetNTrack", &lep1PLTInput_TrackJetNTrack, &b_lep1PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep1PLTInput_DRlj", &lep1PLTInput_DRlj, &b_lep1PLTInput_DRlj);
   fChain->SetBranchAddress("lep1PLTInput_rnnip", &lep1PLTInput_rnnip, &b_lep1PLTInput_rnnip);
   fChain->SetBranchAddress("lep1PLTInput_DL1mu", &lep1PLTInput_DL1mu, &b_lep1PLTInput_DL1mu);
   fChain->SetBranchAddress("lep1PLTInput_PtRel", &lep1PLTInput_PtRel, &b_lep1PLTInput_PtRel);
   fChain->SetBranchAddress("lep1PLTInput_PtFrac", &lep1PLTInput_PtFrac, &b_lep1PLTInput_PtFrac);
   fChain->SetBranchAddress("lep1PLTInput_Topoetcone30Rel", &lep1PLTInput_Topoetcone30Rel, &b_lep1PLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep1PLTInput_Ptvarcone30Rel", &lep1PLTInput_Ptvarcone30Rel, &b_lep1PLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep1CorrPromptLepVeto_score", &lep1CorrPromptLepVeto_score, &b_lep1CorrPromptLepVeto_score);
   fChain->SetBranchAddress("lep1CorrLowPtPLV_score", &lep1CorrLowPtPLV_score, &b_lep1CorrLowPtPLV_score);
   fChain->SetBranchAddress("lep1CorrPLTInput_TrackJetNTrack", &lep1CorrPLTInput_TrackJetNTrack, &b_lep1CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep1CorrPLTInput_DRlj", &lep1CorrPLTInput_DRlj, &b_lep1CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep1CorrPLTInput_rnnip", &lep1CorrPLTInput_rnnip, &b_lep1CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep1CorrPLTInput_DL1mu", &lep1CorrPLTInput_DL1mu, &b_lep1CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep1CorrPLTInput_PtRel", &lep1CorrPLTInput_PtRel, &b_lep1CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep1CorrPLTInput_PtFrac", &lep1CorrPLTInput_PtFrac, &b_lep1CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep1CorrPLTInput_Topoetcone30Rel", &lep1CorrPLTInput_Topoetcone30Rel, &b_lep1CorrPLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep1CorrPLTInput_Ptvarcone30Rel", &lep1CorrPLTInput_Ptvarcone30Rel, &b_lep1CorrPLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep2Flavor", &lep2Flavor, &b_lep2Flavor);
   fChain->SetBranchAddress("lep2OriginFlavor", &lep2OriginFlavor, &b_lep2OriginFlavor);
   fChain->SetBranchAddress("lep2Charge", &lep2Charge, &b_lep2Charge);
   fChain->SetBranchAddress("lep2Author", &lep2Author, &b_lep2Author);
   fChain->SetBranchAddress("lep2Pt", &lep2Pt, &b_lep2Pt);
   fChain->SetBranchAddress("lep2Eta", &lep2Eta, &b_lep2Eta);
   fChain->SetBranchAddress("lep2Phi", &lep2Phi, &b_lep2Phi);
   fChain->SetBranchAddress("lep2M", &lep2M, &b_lep2M);
   fChain->SetBranchAddress("lep2D0", &lep2D0, &b_lep2D0);
   fChain->SetBranchAddress("lep2Z0", &lep2Z0, &b_lep2Z0);
   fChain->SetBranchAddress("lep2D0Sig", &lep2D0Sig, &b_lep2D0Sig);
   fChain->SetBranchAddress("lep2Z0SinTheta", &lep2Z0SinTheta, &b_lep2Z0SinTheta);
   fChain->SetBranchAddress("lep2Z0OriginV", &lep2Z0OriginV, &b_lep2Z0OriginV);
   fChain->SetBranchAddress("lep2OriginVz", &lep2OriginVz, &b_lep2OriginVz);
   fChain->SetBranchAddress("lep2Ptcone20", &lep2Ptcone20, &b_lep2Ptcone20);
   fChain->SetBranchAddress("lep2Ptcone30", &lep2Ptcone30, &b_lep2Ptcone30);
   fChain->SetBranchAddress("lep2Topoetcone20", &lep2Topoetcone20, &b_lep2Topoetcone20);
   fChain->SetBranchAddress("lep2Topoetcone30", &lep2Topoetcone30, &b_lep2Topoetcone30);
   fChain->SetBranchAddress("lep2Ptvarcone20", &lep2Ptvarcone20, &b_lep2Ptvarcone20);
   fChain->SetBranchAddress("lep2Ptvarcone30", &lep2Ptvarcone30, &b_lep2Ptvarcone30);
   fChain->SetBranchAddress("lep2PassOR", &lep2PassOR, &b_lep2PassOR);
   fChain->SetBranchAddress("lep2Type", &lep2Type, &b_lep2Type);
   fChain->SetBranchAddress("lep2Origin", &lep2Origin, &b_lep2Origin);
   fChain->SetBranchAddress("lep2PassBL", &lep2PassBL, &b_lep2PassBL);
   fChain->SetBranchAddress("lep2VeryLoose", &lep2VeryLoose, &b_lep2VeryLoose);
   fChain->SetBranchAddress("lep2Loose", &lep2Loose, &b_lep2Loose);
   fChain->SetBranchAddress("lep2Medium", &lep2Medium, &b_lep2Medium);
   fChain->SetBranchAddress("lep2Tight", &lep2Tight, &b_lep2Tight);
   fChain->SetBranchAddress("lep2LowPtWP", &lep2LowPtWP, &b_lep2LowPtWP);
   fChain->SetBranchAddress("lep2minDRAllJetsLepton", &lep2minDRAllJetsLepton, &b_lep2minDRAllJetsLepton);
   fChain->SetBranchAddress("lep2IsoCorrLoose", &lep2IsoCorrLoose, &b_lep2IsoCorrLoose);
   fChain->SetBranchAddress("lep2IsoCorrTight", &lep2IsoCorrTight, &b_lep2IsoCorrTight);
   fChain->SetBranchAddress("lep2IsoCorrGradient", &lep2IsoCorrGradient, &b_lep2IsoCorrGradient);
   fChain->SetBranchAddress("lep2IsoCorrGradientLoose", &lep2IsoCorrGradientLoose, &b_lep2IsoCorrGradientLoose);
   fChain->SetBranchAddress("lep2IsoCorrLooseTrackOnly", &lep2IsoCorrLooseTrackOnly, &b_lep2IsoCorrLooseTrackOnly);
   fChain->SetBranchAddress("lep2IsoCorrFixedCutLoose", &lep2IsoCorrFixedCutLoose, &b_lep2IsoCorrFixedCutLoose);
   fChain->SetBranchAddress("lep2IsoCorrFixedCutTight", &lep2IsoCorrFixedCutTight, &b_lep2IsoCorrFixedCutTight);
   fChain->SetBranchAddress("lep2IsoCorrFixedCutTightTrackOnly", &lep2IsoCorrFixedCutTightTrackOnly, &b_lep2IsoCorrFixedCutTightTrackOnly);
   fChain->SetBranchAddress("lep2IsoCorrFCTightTrackOnly", &lep2IsoCorrFCTightTrackOnly, &b_lep2IsoCorrFCTightTrackOnly);
   fChain->SetBranchAddress("lep2IsoCorrPLVLoose", &lep2IsoCorrPLVLoose, &b_lep2IsoCorrPLVLoose);
   fChain->SetBranchAddress("lep2IsoCorrPLVTight", &lep2IsoCorrPLVTight, &b_lep2IsoCorrPLVTight);
   fChain->SetBranchAddress("lep2Signal", &lep2Signal, &b_lep2Signal);
   fChain->SetBranchAddress("lep2SignalPLVLoose", &lep2SignalPLVLoose, &b_lep2SignalPLVLoose);
   fChain->SetBranchAddress("lep2SignalPLVTight", &lep2SignalPLVTight, &b_lep2SignalPLVTight);
   fChain->SetBranchAddress("lep2TruthMatched", &lep2TruthMatched, &b_lep2TruthMatched);
   fChain->SetBranchAddress("lep2PromptLepVeto_score", &lep2PromptLepVeto_score, &b_lep2PromptLepVeto_score);
   fChain->SetBranchAddress("lep2LowPtPLV_score", &lep2LowPtPLV_score, &b_lep2LowPtPLV_score);
   fChain->SetBranchAddress("lep2PLTInput_TrackJetNTrack", &lep2PLTInput_TrackJetNTrack, &b_lep2PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep2PLTInput_DRlj", &lep2PLTInput_DRlj, &b_lep2PLTInput_DRlj);
   fChain->SetBranchAddress("lep2PLTInput_rnnip", &lep2PLTInput_rnnip, &b_lep2PLTInput_rnnip);
   fChain->SetBranchAddress("lep2PLTInput_DL1mu", &lep2PLTInput_DL1mu, &b_lep2PLTInput_DL1mu);
   fChain->SetBranchAddress("lep2PLTInput_PtRel", &lep2PLTInput_PtRel, &b_lep2PLTInput_PtRel);
   fChain->SetBranchAddress("lep2PLTInput_PtFrac", &lep2PLTInput_PtFrac, &b_lep2PLTInput_PtFrac);
   fChain->SetBranchAddress("lep2PLTInput_Topoetcone30Rel", &lep2PLTInput_Topoetcone30Rel, &b_lep2PLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep2PLTInput_Ptvarcone30Rel", &lep2PLTInput_Ptvarcone30Rel, &b_lep2PLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep2CorrPromptLepVeto_score", &lep2CorrPromptLepVeto_score, &b_lep2CorrPromptLepVeto_score);
   fChain->SetBranchAddress("lep2CorrLowPtPLV_score", &lep2CorrLowPtPLV_score, &b_lep2CorrLowPtPLV_score);
   fChain->SetBranchAddress("lep2CorrPLTInput_TrackJetNTrack", &lep2CorrPLTInput_TrackJetNTrack, &b_lep2CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep2CorrPLTInput_DRlj", &lep2CorrPLTInput_DRlj, &b_lep2CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep2CorrPLTInput_rnnip", &lep2CorrPLTInput_rnnip, &b_lep2CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep2CorrPLTInput_DL1mu", &lep2CorrPLTInput_DL1mu, &b_lep2CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep2CorrPLTInput_PtRel", &lep2CorrPLTInput_PtRel, &b_lep2CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep2CorrPLTInput_PtFrac", &lep2CorrPLTInput_PtFrac, &b_lep2CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep2CorrPLTInput_Topoetcone30Rel", &lep2CorrPLTInput_Topoetcone30Rel, &b_lep2CorrPLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep2CorrPLTInput_Ptvarcone30Rel", &lep2CorrPLTInput_Ptvarcone30Rel, &b_lep2CorrPLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep3Flavor", &lep3Flavor, &b_lep3Flavor);
   fChain->SetBranchAddress("lep3OriginFlavor", &lep3OriginFlavor, &b_lep3OriginFlavor);
   fChain->SetBranchAddress("lep3Charge", &lep3Charge, &b_lep3Charge);
   fChain->SetBranchAddress("lep3Author", &lep3Author, &b_lep3Author);
   fChain->SetBranchAddress("lep3Pt", &lep3Pt, &b_lep3Pt);
   fChain->SetBranchAddress("lep3Eta", &lep3Eta, &b_lep3Eta);
   fChain->SetBranchAddress("lep3Phi", &lep3Phi, &b_lep3Phi);
   fChain->SetBranchAddress("lep3M", &lep3M, &b_lep3M);
   fChain->SetBranchAddress("lep3D0", &lep3D0, &b_lep3D0);
   fChain->SetBranchAddress("lep3Z0", &lep3Z0, &b_lep3Z0);
   fChain->SetBranchAddress("lep3D0Sig", &lep3D0Sig, &b_lep3D0Sig);
   fChain->SetBranchAddress("lep3Z0SinTheta", &lep3Z0SinTheta, &b_lep3Z0SinTheta);
   fChain->SetBranchAddress("lep3Z0OriginV", &lep3Z0OriginV, &b_lep3Z0OriginV);
   fChain->SetBranchAddress("lep3OriginVz", &lep3OriginVz, &b_lep3OriginVz);
   fChain->SetBranchAddress("lep3Ptcone20", &lep3Ptcone20, &b_lep3Ptcone20);
   fChain->SetBranchAddress("lep3Ptcone30", &lep3Ptcone30, &b_lep3Ptcone30);
   fChain->SetBranchAddress("lep3Topoetcone20", &lep3Topoetcone20, &b_lep3Topoetcone20);
   fChain->SetBranchAddress("lep3Topoetcone30", &lep3Topoetcone30, &b_lep3Topoetcone30);
   fChain->SetBranchAddress("lep3Ptvarcone20", &lep3Ptvarcone20, &b_lep3Ptvarcone20);
   fChain->SetBranchAddress("lep3Ptvarcone30", &lep3Ptvarcone30, &b_lep3Ptvarcone30);
   fChain->SetBranchAddress("lep3PassOR", &lep3PassOR, &b_lep3PassOR);
   fChain->SetBranchAddress("lep3Type", &lep3Type, &b_lep3Type);
   fChain->SetBranchAddress("lep3Origin", &lep3Origin, &b_lep3Origin);
   fChain->SetBranchAddress("lep3PassBL", &lep3PassBL, &b_lep3PassBL);
   fChain->SetBranchAddress("lep3VeryLoose", &lep3VeryLoose, &b_lep3VeryLoose);
   fChain->SetBranchAddress("lep3Loose", &lep3Loose, &b_lep3Loose);
   fChain->SetBranchAddress("lep3Medium", &lep3Medium, &b_lep3Medium);
   fChain->SetBranchAddress("lep3Tight", &lep3Tight, &b_lep3Tight);
   fChain->SetBranchAddress("lep3LowPtWP", &lep3LowPtWP, &b_lep3LowPtWP);
   fChain->SetBranchAddress("lep3minDRAllJetsLepton", &lep3minDRAllJetsLepton, &b_lep3minDRAllJetsLepton);
   fChain->SetBranchAddress("lep3IsoCorrLoose", &lep3IsoCorrLoose, &b_lep3IsoCorrLoose);
   fChain->SetBranchAddress("lep3IsoCorrTight", &lep3IsoCorrTight, &b_lep3IsoCorrTight);
   fChain->SetBranchAddress("lep3IsoCorrGradient", &lep3IsoCorrGradient, &b_lep3IsoCorrGradient);
   fChain->SetBranchAddress("lep3IsoCorrGradientLoose", &lep3IsoCorrGradientLoose, &b_lep3IsoCorrGradientLoose);
   fChain->SetBranchAddress("lep3IsoCorrLooseTrackOnly", &lep3IsoCorrLooseTrackOnly, &b_lep3IsoCorrLooseTrackOnly);
   fChain->SetBranchAddress("lep3IsoCorrFixedCutLoose", &lep3IsoCorrFixedCutLoose, &b_lep3IsoCorrFixedCutLoose);
   fChain->SetBranchAddress("lep3IsoCorrFixedCutTight", &lep3IsoCorrFixedCutTight, &b_lep3IsoCorrFixedCutTight);
   fChain->SetBranchAddress("lep3IsoCorrFixedCutTightTrackOnly", &lep3IsoCorrFixedCutTightTrackOnly, &b_lep3IsoCorrFixedCutTightTrackOnly);
   fChain->SetBranchAddress("lep3IsoCorrFCTightTrackOnly", &lep3IsoCorrFCTightTrackOnly, &b_lep3IsoCorrFCTightTrackOnly);
   fChain->SetBranchAddress("lep3IsoCorrPLVLoose", &lep3IsoCorrPLVLoose, &b_lep3IsoCorrPLVLoose);
   fChain->SetBranchAddress("lep3IsoCorrPLVTight", &lep3IsoCorrPLVTight, &b_lep3IsoCorrPLVTight);
   fChain->SetBranchAddress("lep3Signal", &lep3Signal, &b_lep3Signal);
   fChain->SetBranchAddress("lep3SignalPLVLoose", &lep3SignalPLVLoose, &b_lep3SignalPLVLoose);
   fChain->SetBranchAddress("lep3SignalPLVTight", &lep3SignalPLVTight, &b_lep3SignalPLVTight);
   fChain->SetBranchAddress("lep3TruthMatched", &lep3TruthMatched, &b_lep3TruthMatched);
   fChain->SetBranchAddress("lep3PromptLepVeto_score", &lep3PromptLepVeto_score, &b_lep3PromptLepVeto_score);
   fChain->SetBranchAddress("lep3LowPtPLV_score", &lep3LowPtPLV_score, &b_lep3LowPtPLV_score);
   fChain->SetBranchAddress("lep3PLTInput_TrackJetNTrack", &lep3PLTInput_TrackJetNTrack, &b_lep3PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep3PLTInput_DRlj", &lep3PLTInput_DRlj, &b_lep3PLTInput_DRlj);
   fChain->SetBranchAddress("lep3PLTInput_rnnip", &lep3PLTInput_rnnip, &b_lep3PLTInput_rnnip);
   fChain->SetBranchAddress("lep3PLTInput_DL1mu", &lep3PLTInput_DL1mu, &b_lep3PLTInput_DL1mu);
   fChain->SetBranchAddress("lep3PLTInput_PtRel", &lep3PLTInput_PtRel, &b_lep3PLTInput_PtRel);
   fChain->SetBranchAddress("lep3PLTInput_PtFrac", &lep3PLTInput_PtFrac, &b_lep3PLTInput_PtFrac);
   fChain->SetBranchAddress("lep3PLTInput_Topoetcone30Rel", &lep3PLTInput_Topoetcone30Rel, &b_lep3PLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep3PLTInput_Ptvarcone30Rel", &lep3PLTInput_Ptvarcone30Rel, &b_lep3PLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep3CorrPromptLepVeto_score", &lep3CorrPromptLepVeto_score, &b_lep3CorrPromptLepVeto_score);
   fChain->SetBranchAddress("lep3CorrLowPtPLV_score", &lep3CorrLowPtPLV_score, &b_lep3CorrLowPtPLV_score);
   fChain->SetBranchAddress("lep3CorrPLTInput_TrackJetNTrack", &lep3CorrPLTInput_TrackJetNTrack, &b_lep3CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep3CorrPLTInput_DRlj", &lep3CorrPLTInput_DRlj, &b_lep3CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep3CorrPLTInput_rnnip", &lep3CorrPLTInput_rnnip, &b_lep3CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep3CorrPLTInput_DL1mu", &lep3CorrPLTInput_DL1mu, &b_lep3CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep3CorrPLTInput_PtRel", &lep3CorrPLTInput_PtRel, &b_lep3CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep3CorrPLTInput_PtFrac", &lep3CorrPLTInput_PtFrac, &b_lep3CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep3CorrPLTInput_Topoetcone30Rel", &lep3CorrPLTInput_Topoetcone30Rel, &b_lep3CorrPLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep3CorrPLTInput_Ptvarcone30Rel", &lep3CorrPLTInput_Ptvarcone30Rel, &b_lep3CorrPLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep4Flavor", &lep4Flavor, &b_lep4Flavor);
   fChain->SetBranchAddress("lep4OriginFlavor", &lep4OriginFlavor, &b_lep4OriginFlavor);
   fChain->SetBranchAddress("lep4Charge", &lep4Charge, &b_lep4Charge);
   fChain->SetBranchAddress("lep4Author", &lep4Author, &b_lep4Author);
   fChain->SetBranchAddress("lep4Pt", &lep4Pt, &b_lep4Pt);
   fChain->SetBranchAddress("lep4Eta", &lep4Eta, &b_lep4Eta);
   fChain->SetBranchAddress("lep4Phi", &lep4Phi, &b_lep4Phi);
   fChain->SetBranchAddress("lep4M", &lep4M, &b_lep4M);
   fChain->SetBranchAddress("lep4D0", &lep4D0, &b_lep4D0);
   fChain->SetBranchAddress("lep4Z0", &lep4Z0, &b_lep4Z0);
   fChain->SetBranchAddress("lep4D0Sig", &lep4D0Sig, &b_lep4D0Sig);
   fChain->SetBranchAddress("lep4Z0SinTheta", &lep4Z0SinTheta, &b_lep4Z0SinTheta);
   fChain->SetBranchAddress("lep4Z0OriginV", &lep4Z0OriginV, &b_lep4Z0OriginV);
   fChain->SetBranchAddress("lep4OriginVz", &lep4OriginVz, &b_lep4OriginVz);
   fChain->SetBranchAddress("lep4Ptcone20", &lep4Ptcone20, &b_lep4Ptcone20);
   fChain->SetBranchAddress("lep4Ptcone30", &lep4Ptcone30, &b_lep4Ptcone30);
   fChain->SetBranchAddress("lep4Topoetcone20", &lep4Topoetcone20, &b_lep4Topoetcone20);
   fChain->SetBranchAddress("lep4Topoetcone30", &lep4Topoetcone30, &b_lep4Topoetcone30);
   fChain->SetBranchAddress("lep4Ptvarcone20", &lep4Ptvarcone20, &b_lep4Ptvarcone20);
   fChain->SetBranchAddress("lep4Ptvarcone30", &lep4Ptvarcone30, &b_lep4Ptvarcone30);
   fChain->SetBranchAddress("lep4PassOR", &lep4PassOR, &b_lep4PassOR);
   fChain->SetBranchAddress("lep4Type", &lep4Type, &b_lep4Type);
   fChain->SetBranchAddress("lep4Origin", &lep4Origin, &b_lep4Origin);
   fChain->SetBranchAddress("lep4PassBL", &lep4PassBL, &b_lep4PassBL);
   fChain->SetBranchAddress("lep4VeryLoose", &lep4VeryLoose, &b_lep4VeryLoose);
   fChain->SetBranchAddress("lep4Loose", &lep4Loose, &b_lep4Loose);
   fChain->SetBranchAddress("lep4Medium", &lep4Medium, &b_lep4Medium);
   fChain->SetBranchAddress("lep4Tight", &lep4Tight, &b_lep4Tight);
   fChain->SetBranchAddress("lep4LowPtWP", &lep4LowPtWP, &b_lep4LowPtWP);
   fChain->SetBranchAddress("lep4minDRAllJetsLepton", &lep4minDRAllJetsLepton, &b_lep4minDRAllJetsLepton);
   fChain->SetBranchAddress("lep4IsoCorrLoose", &lep4IsoCorrLoose, &b_lep4IsoCorrLoose);
   fChain->SetBranchAddress("lep4IsoCorrTight", &lep4IsoCorrTight, &b_lep4IsoCorrTight);
   fChain->SetBranchAddress("lep4IsoCorrGradient", &lep4IsoCorrGradient, &b_lep4IsoCorrGradient);
   fChain->SetBranchAddress("lep4IsoCorrGradientLoose", &lep4IsoCorrGradientLoose, &b_lep4IsoCorrGradientLoose);
   fChain->SetBranchAddress("lep4IsoCorrLooseTrackOnly", &lep4IsoCorrLooseTrackOnly, &b_lep4IsoCorrLooseTrackOnly);
   fChain->SetBranchAddress("lep4IsoCorrFixedCutLoose", &lep4IsoCorrFixedCutLoose, &b_lep4IsoCorrFixedCutLoose);
   fChain->SetBranchAddress("lep4IsoCorrFixedCutTight", &lep4IsoCorrFixedCutTight, &b_lep4IsoCorrFixedCutTight);
   fChain->SetBranchAddress("lep4IsoCorrFixedCutTightTrackOnly", &lep4IsoCorrFixedCutTightTrackOnly, &b_lep4IsoCorrFixedCutTightTrackOnly);
   fChain->SetBranchAddress("lep4IsoCorrFCTightTrackOnly", &lep4IsoCorrFCTightTrackOnly, &b_lep4IsoCorrFCTightTrackOnly);
   fChain->SetBranchAddress("lep4IsoCorrPLVLoose", &lep4IsoCorrPLVLoose, &b_lep4IsoCorrPLVLoose);
   fChain->SetBranchAddress("lep4IsoCorrPLVTight", &lep4IsoCorrPLVTight, &b_lep4IsoCorrPLVTight);
   fChain->SetBranchAddress("lep4Signal", &lep4Signal, &b_lep4Signal);
   fChain->SetBranchAddress("lep4SignalPLVLoose", &lep4SignalPLVLoose, &b_lep4SignalPLVLoose);
   fChain->SetBranchAddress("lep4SignalPLVTight", &lep4SignalPLVTight, &b_lep4SignalPLVTight);
   fChain->SetBranchAddress("lep4TruthMatched", &lep4TruthMatched, &b_lep4TruthMatched);
   fChain->SetBranchAddress("lep4PromptLepVeto_score", &lep4PromptLepVeto_score, &b_lep4PromptLepVeto_score);
   fChain->SetBranchAddress("lep4LowPtPLV_score", &lep4LowPtPLV_score, &b_lep4LowPtPLV_score);
   fChain->SetBranchAddress("lep4PLTInput_TrackJetNTrack", &lep4PLTInput_TrackJetNTrack, &b_lep4PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep4PLTInput_DRlj", &lep4PLTInput_DRlj, &b_lep4PLTInput_DRlj);
   fChain->SetBranchAddress("lep4PLTInput_rnnip", &lep4PLTInput_rnnip, &b_lep4PLTInput_rnnip);
   fChain->SetBranchAddress("lep4PLTInput_DL1mu", &lep4PLTInput_DL1mu, &b_lep4PLTInput_DL1mu);
   fChain->SetBranchAddress("lep4PLTInput_PtRel", &lep4PLTInput_PtRel, &b_lep4PLTInput_PtRel);
   fChain->SetBranchAddress("lep4PLTInput_PtFrac", &lep4PLTInput_PtFrac, &b_lep4PLTInput_PtFrac);
   fChain->SetBranchAddress("lep4PLTInput_Topoetcone30Rel", &lep4PLTInput_Topoetcone30Rel, &b_lep4PLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep4PLTInput_Ptvarcone30Rel", &lep4PLTInput_Ptvarcone30Rel, &b_lep4PLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("lep4CorrPromptLepVeto_score", &lep4CorrPromptLepVeto_score, &b_lep4CorrPromptLepVeto_score);
   fChain->SetBranchAddress("lep4CorrLowPtPLV_score", &lep4CorrLowPtPLV_score, &b_lep4CorrLowPtPLV_score);
   fChain->SetBranchAddress("lep4CorrPLTInput_TrackJetNTrack", &lep4CorrPLTInput_TrackJetNTrack, &b_lep4CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep4CorrPLTInput_DRlj", &lep4CorrPLTInput_DRlj, &b_lep4CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep4CorrPLTInput_rnnip", &lep4CorrPLTInput_rnnip, &b_lep4CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep4CorrPLTInput_DL1mu", &lep4CorrPLTInput_DL1mu, &b_lep4CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep4CorrPLTInput_PtRel", &lep4CorrPLTInput_PtRel, &b_lep4CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep4CorrPLTInput_PtFrac", &lep4CorrPLTInput_PtFrac, &b_lep4CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep4CorrPLTInput_Topoetcone30Rel", &lep4CorrPLTInput_Topoetcone30Rel, &b_lep4CorrPLTInput_Topoetcone30Rel);
   fChain->SetBranchAddress("lep4CorrPLTInput_Ptvarcone30Rel", &lep4CorrPLTInput_Ptvarcone30Rel, &b_lep4CorrPLTInput_Ptvarcone30Rel);
   fChain->SetBranchAddress("N2_particle0_Flavor", &N2_particle0_Flavor, &b_N2_particle0_Flavor);
   fChain->SetBranchAddress("N2_particle1_Flavor", &N2_particle1_Flavor, &b_N2_particle1_Flavor);
   fChain->SetBranchAddress("N2_particle2_Flavor", &N2_particle2_Flavor, &b_N2_particle2_Flavor);
   fChain->SetBranchAddress("N2_particle3_Flavor", &N2_particle3_Flavor, &b_N2_particle3_Flavor);
   fChain->SetBranchAddress("N2_particle0_Eta", &N2_particle0_Eta, &b_N2_particle0_Eta);
   fChain->SetBranchAddress("N2_particle1_Eta", &N2_particle1_Eta, &b_N2_particle1_Eta);
   fChain->SetBranchAddress("N2_particle2_Eta", &N2_particle2_Eta, &b_N2_particle2_Eta);
   fChain->SetBranchAddress("N2_particle3_Eta", &N2_particle3_Eta, &b_N2_particle3_Eta);
   fChain->SetBranchAddress("N2_particle0_Phi", &N2_particle0_Phi, &b_N2_particle0_Phi);
   fChain->SetBranchAddress("N2_particle1_Phi", &N2_particle1_Phi, &b_N2_particle1_Phi);
   fChain->SetBranchAddress("N2_particle2_Phi", &N2_particle2_Phi, &b_N2_particle2_Phi);
   fChain->SetBranchAddress("N2_particle3_Phi", &N2_particle3_Phi, &b_N2_particle3_Phi);
   fChain->SetBranchAddress("N2_particle0_pT", &N2_particle0_pT, &b_N2_particle0_pT);
   fChain->SetBranchAddress("N2_particle1_pT", &N2_particle1_pT, &b_N2_particle1_pT);
   fChain->SetBranchAddress("N2_particle2_pT", &N2_particle2_pT, &b_N2_particle2_pT);
   fChain->SetBranchAddress("N2_particle3_pT", &N2_particle3_pT, &b_N2_particle3_pT);
   fChain->SetBranchAddress("N2_particle0_VtxX", &N2_particle0_VtxX, &b_N2_particle0_VtxX);
   fChain->SetBranchAddress("N2_particle1_VtxX", &N2_particle1_VtxX, &b_N2_particle1_VtxX);
   fChain->SetBranchAddress("N2_particle2_VtxX", &N2_particle2_VtxX, &b_N2_particle2_VtxX);
   fChain->SetBranchAddress("N2_particle3_VtxX", &N2_particle3_VtxX, &b_N2_particle3_VtxX);
   fChain->SetBranchAddress("N2_particle0_VtxY", &N2_particle0_VtxY, &b_N2_particle0_VtxY);
   fChain->SetBranchAddress("N2_particle1_VtxY", &N2_particle1_VtxY, &b_N2_particle1_VtxY);
   fChain->SetBranchAddress("N2_particle2_VtxY", &N2_particle2_VtxY, &b_N2_particle2_VtxY);
   fChain->SetBranchAddress("N2_particle3_VtxY", &N2_particle3_VtxY, &b_N2_particle3_VtxY);
   fChain->SetBranchAddress("N2_particle0_VtxZ", &N2_particle0_VtxZ, &b_N2_particle0_VtxZ);
   fChain->SetBranchAddress("N2_particle1_VtxZ", &N2_particle1_VtxZ, &b_N2_particle1_VtxZ);
   fChain->SetBranchAddress("N2_particle2_VtxZ", &N2_particle2_VtxZ, &b_N2_particle2_VtxZ);
   fChain->SetBranchAddress("N2_particle3_VtxZ", &N2_particle3_VtxZ, &b_N2_particle3_VtxZ);
   fChain->SetBranchAddress("C1_particle0_Flavor", &C1_particle0_Flavor, &b_C1_particle0_Flavor);
   fChain->SetBranchAddress("C1_particle1_Flavor", &C1_particle1_Flavor, &b_C1_particle1_Flavor);
   fChain->SetBranchAddress("C1_particle2_Flavor", &C1_particle2_Flavor, &b_C1_particle2_Flavor);
   fChain->SetBranchAddress("C1_particle3_Flavor", &C1_particle3_Flavor, &b_C1_particle3_Flavor);
   fChain->SetBranchAddress("C1_particle0_Eta", &C1_particle0_Eta, &b_C1_particle0_Eta);
   fChain->SetBranchAddress("C1_particle1_Eta", &C1_particle1_Eta, &b_C1_particle1_Eta);
   fChain->SetBranchAddress("C1_particle2_Eta", &C1_particle2_Eta, &b_C1_particle2_Eta);
   fChain->SetBranchAddress("C1_particle3_Eta", &C1_particle3_Eta, &b_C1_particle3_Eta);
   fChain->SetBranchAddress("C1_particle0_Phi", &C1_particle0_Phi, &b_C1_particle0_Phi);
   fChain->SetBranchAddress("C1_particle1_Phi", &C1_particle1_Phi, &b_C1_particle1_Phi);
   fChain->SetBranchAddress("C1_particle2_Phi", &C1_particle2_Phi, &b_C1_particle2_Phi);
   fChain->SetBranchAddress("C1_particle3_Phi", &C1_particle3_Phi, &b_C1_particle3_Phi);
   fChain->SetBranchAddress("C1_particle0_pT", &C1_particle0_pT, &b_C1_particle0_pT);
   fChain->SetBranchAddress("C1_particle1_pT", &C1_particle1_pT, &b_C1_particle1_pT);
   fChain->SetBranchAddress("C1_particle2_pT", &C1_particle2_pT, &b_C1_particle2_pT);
   fChain->SetBranchAddress("C1_particle3_pT", &C1_particle3_pT, &b_C1_particle3_pT);
   fChain->SetBranchAddress("C1_particle0_VtxX", &C1_particle0_VtxX, &b_C1_particle0_VtxX);
   fChain->SetBranchAddress("C1_particle1_VtxX", &C1_particle1_VtxX, &b_C1_particle1_VtxX);
   fChain->SetBranchAddress("C1_particle2_VtxX", &C1_particle2_VtxX, &b_C1_particle2_VtxX);
   fChain->SetBranchAddress("C1_particle3_VtxX", &C1_particle3_VtxX, &b_C1_particle3_VtxX);
   fChain->SetBranchAddress("C1_particle0_VtxY", &C1_particle0_VtxY, &b_C1_particle0_VtxY);
   fChain->SetBranchAddress("C1_particle1_VtxY", &C1_particle1_VtxY, &b_C1_particle1_VtxY);
   fChain->SetBranchAddress("C1_particle2_VtxY", &C1_particle2_VtxY, &b_C1_particle2_VtxY);
   fChain->SetBranchAddress("C1_particle3_VtxY", &C1_particle3_VtxY, &b_C1_particle3_VtxY);
   fChain->SetBranchAddress("C1_particle0_VtxZ", &C1_particle0_VtxZ, &b_C1_particle0_VtxZ);
   fChain->SetBranchAddress("C1_particle1_VtxZ", &C1_particle1_VtxZ, &b_C1_particle1_VtxZ);
   fChain->SetBranchAddress("C1_particle2_VtxZ", &C1_particle2_VtxZ, &b_C1_particle2_VtxZ);
   fChain->SetBranchAddress("C1_particle3_VtxZ", &C1_particle3_VtxZ, &b_C1_particle3_VtxZ);
   fChain->SetBranchAddress("TrackEta", &TrackEta, &b_TrackEta);
   fChain->SetBranchAddress("TrackPhi", &TrackPhi, &b_TrackPhi);
   fChain->SetBranchAddress("TrackPt", &TrackPt, &b_TrackPt);
   fChain->SetBranchAddress("TrackCharge", &TrackCharge, &b_TrackCharge);
   fChain->SetBranchAddress("TrackOriginFlavor", &TrackOriginFlavor, &b_TrackOriginFlavor);
   fChain->SetBranchAddress("TrackFlavor", &TrackFlavor, &b_TrackFlavor);
   fChain->SetBranchAddress("TrackD0", &TrackD0, &b_TrackD0);
   fChain->SetBranchAddress("TrackZ0", &TrackZ0, &b_TrackZ0);
   fChain->SetBranchAddress("TrackZ0OriginV", &TrackZ0OriginV, &b_TrackZ0OriginV);
   fChain->SetBranchAddress("TrackOriginVz", &TrackOriginVz, &b_TrackOriginVz);
   fChain->SetBranchAddress("TrackZ0_CVz", &TrackZ0_CVz, &b_TrackZ0_CVz);
   fChain->SetBranchAddress("TrackD0Err", &TrackD0Err, &b_TrackD0Err);
   fChain->SetBranchAddress("TrackZ0Err", &TrackZ0Err, &b_TrackZ0Err);
   fChain->SetBranchAddress("TrackPtcone20", &TrackPtcone20, &b_TrackPtcone20);
   fChain->SetBranchAddress("TrackPtcone30", &TrackPtcone30, &b_TrackPtcone30);
   fChain->SetBranchAddress("TrackPtcone40", &TrackPtcone40, &b_TrackPtcone40);
   fChain->SetBranchAddress("etcone20Topo", &etcone20Topo, &b_etcone20Topo);
   fChain->SetBranchAddress("etclus40Topo", &etclus40Topo, &b_etclus40Topo);
   fChain->SetBranchAddress("nIBLHits", &nIBLHits, &b_nIBLHits);
   fChain->SetBranchAddress("nPixLayers", &nPixLayers, &b_nPixLayers);
   fChain->SetBranchAddress("nExpBLayerHits", &nExpBLayerHits, &b_nExpBLayerHits);
   fChain->SetBranchAddress("nPixHits", &nPixHits, &b_nPixHits);
   fChain->SetBranchAddress("nPixHoles", &nPixHoles, &b_nPixHoles);
   fChain->SetBranchAddress("nPixOutliers", &nPixOutliers, &b_nPixOutliers);
   fChain->SetBranchAddress("nSCTHits", &nSCTHits, &b_nSCTHits);
   fChain->SetBranchAddress("nSCTHoles", &nSCTHoles, &b_nSCTHoles);
   fChain->SetBranchAddress("nSCTSharedHits", &nSCTSharedHits, &b_nSCTSharedHits);
   fChain->SetBranchAddress("nSCTOutliers", &nSCTOutliers, &b_nSCTOutliers);
   fChain->SetBranchAddress("nTRTHits", &nTRTHits, &b_nTRTHits);
   fChain->SetBranchAddress("nPixSpoiltHits", &nPixSpoiltHits, &b_nPixSpoiltHits);
   fChain->SetBranchAddress("nGangedFlaggedFakes", &nGangedFlaggedFakes, &b_nGangedFlaggedFakes);
   fChain->SetBranchAddress("eProbabilityComb", &eProbabilityComb, &b_eProbabilityComb);
   fChain->SetBranchAddress("eProbabilityHT", &eProbabilityHT, &b_eProbabilityHT);
   fChain->SetBranchAddress("pixeldEdx", &pixeldEdx, &b_pixeldEdx);
   fChain->SetBranchAddress("nJet30", &nJet30, &b_nJet30);
   fChain->SetBranchAddress("nJet25", &nJet25, &b_nJet25);
   fChain->SetBranchAddress("nJet20", &nJet20, &b_nJet20);
   fChain->SetBranchAddress("nTotalJet", &nTotalJet, &b_nTotalJet);
   fChain->SetBranchAddress("nTotalJet20", &nTotalJet20, &b_nTotalJet20);
   fChain->SetBranchAddress("nBJet20_MV2c10", &nBJet20_MV2c10, &b_nBJet20_MV2c10);
   fChain->SetBranchAddress("nBJet30_MV2c10", &nBJet30_MV2c10, &b_nBJet30_MV2c10);
   fChain->SetBranchAddress("nBJet20_MV2c10_FixedCutBEff_60", &nBJet20_MV2c10_FixedCutBEff_60, &b_nBJet20_MV2c10_FixedCutBEff_60);
   fChain->SetBranchAddress("nBJet20_MV2c10_FixedCutBEff_70", &nBJet20_MV2c10_FixedCutBEff_70, &b_nBJet20_MV2c10_FixedCutBEff_70);
   fChain->SetBranchAddress("nBJet20_MV2c10_FixedCutBEff_77", &nBJet20_MV2c10_FixedCutBEff_77, &b_nBJet20_MV2c10_FixedCutBEff_77);
   fChain->SetBranchAddress("nBJet20_MV2c10_FixedCutBEff_85", &nBJet20_MV2c10_FixedCutBEff_85, &b_nBJet20_MV2c10_FixedCutBEff_85);
   fChain->SetBranchAddress("nBJet30_MV2c10_FixedCutBEff_60", &nBJet30_MV2c10_FixedCutBEff_60, &b_nBJet30_MV2c10_FixedCutBEff_60);
   fChain->SetBranchAddress("nBJet30_MV2c10_FixedCutBEff_70", &nBJet30_MV2c10_FixedCutBEff_70, &b_nBJet30_MV2c10_FixedCutBEff_70);
   fChain->SetBranchAddress("nBJet30_MV2c10_FixedCutBEff_77", &nBJet30_MV2c10_FixedCutBEff_77, &b_nBJet30_MV2c10_FixedCutBEff_77);
   fChain->SetBranchAddress("nBJet30_MV2c10_FixedCutBEff_85", &nBJet30_MV2c10_FixedCutBEff_85, &b_nBJet30_MV2c10_FixedCutBEff_85);
   fChain->SetBranchAddress("jet1Pt", &jet1Pt, &b_jet1Pt);
   fChain->SetBranchAddress("jet1Phi", &jet1Phi, &b_jet1Phi);
   fChain->SetBranchAddress("jetPt", &jetPt, &b_jetPt);
   fChain->SetBranchAddress("jetEta", &jetEta, &b_jetEta);
   fChain->SetBranchAddress("jetPhi", &jetPhi, &b_jetPhi);
   fChain->SetBranchAddress("jetM", &jetM, &b_jetM);
   fChain->SetBranchAddress("jetnTrk", &jetnTrk, &b_jetnTrk);
   fChain->SetBranchAddress("jetTileEnergy", &jetTileEnergy, &b_jetTileEnergy);
   fChain->SetBranchAddress("ajetPt", &ajetPt, &b_ajetPt);
   fChain->SetBranchAddress("ajetEta", &ajetEta, &b_ajetEta);
   fChain->SetBranchAddress("ajetPhi", &ajetPhi, &b_ajetPhi);
   fChain->SetBranchAddress("ajetM", &ajetM, &b_ajetM);
   fChain->SetBranchAddress("ajetnTrk", &ajetnTrk, &b_ajetnTrk);
   fChain->SetBranchAddress("ajetTileEnergy", &ajetTileEnergy, &b_ajetTileEnergy);
   fChain->SetBranchAddress("met_Et", &met_Et, &b_met_Et);
   fChain->SetBranchAddress("met_px", &met_px, &b_met_px);
   fChain->SetBranchAddress("met_py", &met_py, &b_met_py);
   fChain->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
   fChain->SetBranchAddress("met_pz", &met_pz, &b_met_pz);
   fChain->SetBranchAddress("met_Signif", &met_Signif, &b_met_Signif);
   fChain->SetBranchAddress("mt_lep1", &mt_lep1, &b_mt_lep1);
   fChain->SetBranchAddress("METOverHT", &METOverHT, &b_METOverHT);
   fChain->SetBranchAddress("DPhiJ1Met", &DPhiJ1Met, &b_DPhiJ1Met);
   fChain->SetBranchAddress("minDPhiAllJetsMet", &minDPhiAllJetsMet, &b_minDPhiAllJetsMet);
   fChain->SetBranchAddress("METOverHTLep", &METOverHTLep, &b_METOverHTLep);
   fChain->SetBranchAddress("mll", &mll, &b_mll);
   fChain->SetBranchAddress("Rll", &Rll, &b_Rll);
   fChain->SetBranchAddress("MTauTau", &MTauTau, &b_MTauTau);
   fChain->SetBranchAddress("mt2leplsp_100", &mt2leplsp_100, &b_mt2leplsp_100);
   fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
   fChain->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
   fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
   fChain->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
   fChain->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
   fChain->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
   fChain->SetBranchAddress("genWeightUp", &genWeightUp, &b_genWeightUp);
   fChain->SetBranchAddress("genWeightDown", &genWeightDown, &b_genWeightDown);
   fChain->SetBranchAddress("truthMll", &truthMll, &b_truthMll);
   fChain->SetBranchAddress("winoBinoMllWeight", &winoBinoMllWeight, &b_winoBinoMllWeight);
   fChain->SetBranchAddress("FFWeight", &FFWeight, &b_FFWeight);
   fChain->SetBranchAddress("nLep_antiID", &nLep_antiID, &b_nLep_antiID);
   fChain->SetBranchAddress("lep1AntiID", &lep1AntiID, &b_lep1AntiID);
   fChain->SetBranchAddress("lep2AntiID", &lep2AntiID, &b_lep2AntiID);
   fChain->SetBranchAddress("nLep_signalActual", &nLep_signalActual, &b_nLep_signalActual);
   fChain->SetBranchAddress("lep1SignalActual", &lep1SignalActual, &b_lep1SignalActual);
   fChain->SetBranchAddress("lep2SignalActual", &lep2SignalActual, &b_lep2SignalActual);
   fChain->SetBranchAddress("ETMissTrigSF", &ETMissTrigSF, &b_ETMissTrigSF);
   fChain->SetBranchAddress("RJR_RISR", &RJR_RISR, &b_RJR_RISR);
   fChain->SetBranchAddress("RJR_MS", &RJR_MS, &b_RJR_MS);
   fChain->SetBranchAddress("RJR_dphiISRI", &RJR_dphiISRI, &b_RJR_dphiISRI);
   fChain->SetBranchAddress("SUSYPt", &SUSYPt, &b_SUSYPt);
   fChain->SetBranchAddress("ISRWeight", &ISRWeight, &b_ISRWeight);
   fChain->SetBranchAddress("PRWHash", &PRWHash, &b_PRWHash);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
   fChain->SetBranchAddress("GenHt", &GenHt, &b_GenHt);
   fChain->SetBranchAddress("GenMET", &GenMET, &b_GenMET);
   fChain->SetBranchAddress("DatasetNumber", &DatasetNumber, &b_DatasetNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
   fChain->SetBranchAddress("FS", &FS, &b_FS);
   Notify();
}

Bool_t tree_NoSys::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tree_NoSys::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tree_NoSys::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef tree_NoSys_cxx
