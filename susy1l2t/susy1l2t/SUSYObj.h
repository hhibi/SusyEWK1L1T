#ifndef SUSYOBJ_h
#define SUSYOBJ_h

#include <vector>

#include "TrackVariable.h"
#include "LeptonVariable.h"
class LeptonVariable;
class TrackVariable;
class TLorentzVector;

class SUSYObj{

 public:
  SUSYObj();

  void erase();
  //---------------------
  //    General Info
  //--------------------

  bool isLeptonicDecay() const;

  bool isReco_OSLepton(bool IsSigLepA, bool IsSigLepB)const;
  bool isReco_OSLeptonTrack(bool IsSigLepA, bool IsSigTrackB)const;
  bool isReco_OSTrack(bool IsSigTrackA, bool IsSigTrackB)const;

  template<typename T,typename U>
    bool isReco_OS(std::vector<T*> trackA,bool isSignalReqA,std::vector<U*> trackB,bool isSignalReqB)const;
  //---------------------
  //    LSP Info
  //--------------------

  //--getter--
  TLorentzVector getLSP()const{ return m_LSP;}

  //--setter--
  void setLSP(TLorentzVector particleVector){m_LSP=particleVector;}

  //---------------------
  //    Lepton Info
  //--------------------

  //-- getter--
  LeptonVariable* getLepton(int index)const{return (*m_Lep)[index];}
  std::vector<LeptonVariable*> getLeptons()const{return *m_Lep;}

  int getNumberOfLepton()const{return m_Lep->size();}  
  int getNumberOfSignalLepton()const{return m_NumberOfSignalLepton;}  


  //--setter--
  void setLepton(LeptonVariable* lep){m_Lep->push_back(lep);}
  void setNumberOfSignalLepton(int Num){m_NumberOfSignalLepton=Num;} 


  //---------------------
  //    Track Info
  //--------------------

  //--getter--
  TrackVariable* getTrackParticle(int index)const{ return (*m_TrackParticle)[index];}
  int getNumberOfTrack()const{return m_NumberOfTrack;} 

  //--setter--
  void setTrackParticle(TrackVariable* particleVector){m_TrackParticle->push_back(particleVector);}
  void setNumberOfTrack(int Num){m_NumberOfTrack=Num;} 


  //-----------------------------
  //    Truth Particle Info
  //----------------------------

  //--getter--
  TrackVariable* getTruthParticle(int index)const{return (*m_TruthParticle)[index];}
  TrackVariable* getLeptonTruthParticle()const; // forChargino
  int getNumberOfTruthParticle()const{return m_TruthParticle->size();}

  //--setter--
  void setTruthParticle(TrackVariable* particleVector){m_TruthParticle->push_back(particleVector);}




  void Print()const;


 private:
  TLorentzVector m_LSP;
  std::vector<TrackVariable*> *m_TrackParticle;
  std::vector<LeptonVariable*> *m_Lep; 
  int m_NumberOfSignalLepton;
  int m_NumberOfTrack;

  std::vector<TrackVariable*> *m_TruthParticle; 


};

#endif
