#ifndef BRANCHMANAGER_h
#define BRANCHMANAGER_h


#include <iostream>
#include <vector>

class TFile;
class TTree;


class BranchManager{

 public:
  BranchManager(std::string FILE, std::string CombiSign);
  ~BranchManager();

  void Write();
  void Draw();

  TFile *file;
  TTree *Tracktree;

  //:::::: variavles :::::::::

  /////////////////////
  ////  Combi Tree
  ////////////////////
  TTree *CombitreeS;
  TTree *CombitreeB;

  double trackEta;//
  double trackPhi;//
  double trackPt;//
  double trackP;//
  double trackZ0;//
  double trackZ0Err;//
  double trackD0;//
  double trackZ0fromOriginV;//
  double trackOriginVz;//
  double trackZ0fromClosestV;//
  double trackPtcone20;//   
  double trackPtcone30;//   
  double trackPtcone40;//   
  double trackEtcone20Topo;//    
  double trackEtcone40Topo;//
  double trackPtcone20Corr;//   
  double trackPtcone30Corr;//   
  double trackPtcone40Corr;//   
  double trackEtcone20TopoCorr;//    
  double trackEtcone40TopoCorr;//
  double leptonEta;//
  double leptonPhi;//
  double leptonPt;//
  double leptonP;//
  int    leptonFlavor;//
  double leptonZ0;//
  double leptonD0;//
  double leptonZ0fromOriginV;//
  double leptonOriginVz;//
  double ZBosonEta;//
  double ZBosonPhi;//
  double ZBosonPt;//
  double ZBosonP;//
  double ZBosonM;//
  double ZBosonBeta;//
  double ZBoson_dRLepTrk;//
  int    ZBosonDecayFlavor;//
  double ZBosonZ0;//
  double ZBosonD0;//
  double lowerPt;//
  double higherPt;//
  double Hleps;//
  double MetPhi;//
  double MetEt;//
  double METoverHleps;//
  double JetPhi;//
  double JetPt;//
  double DPhi_METZ;//
  double DPhi_JETZ;//
  double minDR_TrkJet;//
  double minDR_LepJet;//
  double DeltaMass;//
  double BDTBScore;//
  double PLVScore;//
  double LowPtPLVScore;//
  double LowPtPLVScoreCorr;//


  std::vector<double> TracksEta;
  std::vector<double> TracksPhi;
  std::vector<double> TracksPt;
  std::vector<double> TracksDR_Trk;
  std::vector<double> TracksDR_Lep;
  std::vector<double> TracksZ0;
  std::vector<double> TracksD0;

  double ZIsoPt[10];//
  double ZIsoN[10];//
  double LepIsoPt[10];//
  double LepIsoN[10];//
  double TrkIsoPt[10];//
  double TrkIsoN[10];//

  double test=-99;
  double test2=-99;



  double WEIGHT;
  double dR_SF;


  /////////////////////////////
  ////  UnRecnstracted Event
  ////////////////////////////
  TTree *treeS;
  TTree *treeB;



 private:

  std::string m_FileName;


};

#endif
